#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import argparse
import sys
import time
import json
import zmq

from google.protobuf import json_format
import pmt
from marmote3 import marmote3_pb2


class ModemCli(object):

    def __init__(self, args):
        parser = argparse.ArgumentParser(prog="modem_cli.py",
                                         formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument("command", help="""one of the following commands:
    set_center_freq, set_rf_bandwidth, set_tx_gain, set_rx_gain, 
    set_tx_power, set_active_channels, set_mcs_assignment, 
    set_mandates, set_invert_spectrum, set_enable_recording, 
    set_enable_arq, set_inhibits, read_packets, read_reports""")
        opts = parser.parse_args(args[:1])

        set_cmds = {
            "set_center_freq": ["Hz", "float"],
            "set_rf_bandwidth": ["Hz", "float"],
            "set_rx_gain": ["dB", "float"],
            "set_tx_gain": ["dB", "float"],
            "set_tx_power": ["dB", "float"],
            "set_active_channels": ["chan", "int*"],
            "set_mcs_assignment": ["dst/mcs", "int*"],
            "set_mandates": ["mand", "float*"],
            "set_invert_spectrum": ["flag", "int"],
            "set_enable_recording": ["flag", "int"],
            "set_enable_arq": ["flag", "int"],
            "set_inhibits": ["time/dur", "float*"]
        }
        if opts.command in set_cmds:
            self.zmq_set_parameters(
                args[1:], opts.command, set_cmds[opts.command])
        elif opts.command == "read_packets":
            self.zmq_read_packets(args[1:])
        elif opts.command == "read_reports":
            self.zmq_read_reports(args[1:])
        else:
            print("Unrecognized command: {}".format(opts.command))

    def zmq_set_parameters(self, args, cmd, params):
        parser = argparse.ArgumentParser(prog="modem_cli.py " + cmd,
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument(
            "--addr", default="tcp://127.0.0.1:7555", help="ZMQ socket address")
        for i in range(0, len(params), 2):
            n, t = params[i], params[i + 1]
            if t == "int":
                parser.add_argument(n, type=int)
            elif t == "float":
                parser.add_argument(n, type=float)
            elif t == "int*":
                parser.add_argument(n, nargs="*", type=int)
            elif t == "int2":
                parser.add_argument(n, nargs=2, type=int)
            elif t == "float*":
                parser.add_argument(n, nargs="*", type=float)
            else:
                raise ValueError("invalid type: {}".format(t))
        opts = parser.parse_args(args)

        context = zmq.Context()
        socket = context.socket(zmq.PUSH)
        socket.connect(opts.addr)

        msg = marmote3_pb2.ModemCommand()
        if cmd == "set_center_freq":
            msg.modem_control.center_freq.value = opts.Hz
        elif cmd == "set_rf_bandwidth":
            msg.modem_control.rf_bandwidth.value = opts.Hz
        elif cmd == "set_rx_gain":
            msg.modem_control.rx_gain.value = opts.dB
        elif cmd == "set_tx_gain":
            msg.modem_control.tx_gain.value = opts.dB
        elif cmd == "set_tx_power":
            msg.modem_control.tx_power.value = opts.dB
        elif cmd == "set_invert_spectrum":
            msg.modem_control.invert_spectrum.value = opts.flag != 0
        elif cmd == "set_enable_recording":
            msg.modem_control.enable_recording.value = opts.flag != 0
        elif cmd == "set_enable_arq":
            msg.modem_control.enable_arq.value = opts.flag != 0
        elif cmd == "set_active_channels":
            msg.subcarrier_allocator.active_channels[:] = opts.chan
        elif cmd == "set_mcs_assignment":
            pairs = getattr(opts, "dst/mcs")
            msg.set_mcs.SetInParent()
            for i in range(0, len(pairs) - 1, 2):
                msg.set_mcs.assignment.add(
                    destination=pairs[i], mcs=pairs[i+1])
        elif cmd == "set_mandates":
            tuples = opts.mand
            msg.set_mandates.SetInParent()  # force the message to be present
            for i in range(0, len(tuples) - 4, 5):
                entry = msg.set_mandates.mandates.add()
                entry.flowid = int(tuples[i])
                entry.max_latency_s = tuples[i + 1]
                entry.max_throughput_bps = tuples[i + 2]
                entry.is_file_mandate = tuples[i + 3] != 0
                entry.resend_count = int(tuples[i + 4])
        elif cmd == "set_inhibits":
            current_time = time.time()
            pairs = getattr(opts, "time/dur")
            msg.subcarrier_inhibit.SetInParent()
            for i in range(0, len(pairs) - 1, 2):
                msg.subcarrier_inhibit.inhibits.add(
                    start_time=current_time + pairs[i],
                    duration=pairs[i + 1])
        else:
            print("Missing command")

        socket.send(msg.SerializeToString())

        text = json_format.MessageToJson(msg, preserving_proto_field_name=True)
        print("ZMQ message sent to {}".format(opts.addr))
        print(json.dumps(json.loads(text), indent=2, sort_keys=True))

        socket.close()

    def zmq_read_packets(self, args):
        parser = argparse.ArgumentParser(prog="modem_cli.py read_packets",
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument(
            "--addr", default="tcp://127.0.0.1:7556", help="ZMQ socket address")
        parser.add_argument(
            "--type", choices=["SUB", "PULL"], default="SUB", help="ZMQ socket type")
        opts = parser.parse_args(args)

        context = zmq.Context()
        socket = context.socket(zmq.SUB if opts.type == "SUB" else zmq.PULL)
        if opts.type == "SUB":
            socket.setsockopt(zmq.SUBSCRIBE, "")
        socket.connect(opts.addr)
        print("Reading ZMQ messages from {}".format(opts.addr))

        while True:
            try:
                msg = socket.recv()
            except KeyboardInterrupt:
                break

            try:
                msg = pmt.deserialize_str(msg)
            except Exception as err:
                print("message decode error {}".format(err))
                break

            print(msg)

        socket.close()
        print("\nConnection closed")

    def zmq_read_reports(self, args):
        parser = argparse.ArgumentParser(prog="modem_cli.py read_reports",
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument(
            "--addr", default="tcp://127.0.0.1:7557", help="ZMQ socket address")
        parser.add_argument(
            "--type", choices=["SUB", "PULL"], default="SUB", help="ZMQ socket type")
        opts = parser.parse_args(args)

        context = zmq.Context()
        socket = context.socket(zmq.SUB if opts.type == "SUB" else zmq.PULL)
        if opts.type == "SUB":
            socket.setsockopt(zmq.SUBSCRIBE, "")
        socket.connect(opts.addr)
        print("Reading ZMQ messages from {}".format(opts.addr))

        while True:
            try:
                msg_raw = socket.recv()
            except KeyboardInterrupt:
                break

            try:
                msg = marmote3_pb2.ModemReport.FromString(msg_raw)
            except Exception as err:
                print("message decode error {}".format(err))
                break

            msg = json_format.MessageToJson(
                msg, preserving_proto_field_name=True)
            print(json.dumps(json.loads(msg), indent=2, sort_keys=True), '\n')

        socket.close()
        print("\nConnection closed")


if __name__ == "__main__":
    ModemCli(sys.argv[1:])
