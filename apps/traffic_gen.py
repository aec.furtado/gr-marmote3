#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function

import argparse
import fcntl
import logging
import random
import socket
import struct
import threading
import time
import zlib


IF_NAME = 'tap0'


class Generator(object):

    def __init__(self,
                 radio_id,
                 broadcast_addr,
                 broadcast_port,
                 send_rate,
                 length_min,
                 length_max,
                 destination_addr):
        self.log = logging.getLogger("generator")

        if not (5 <= length_min <= length_max <= 65507):
            raise ValueError("invalid min or max length")
        self.length_min = length_min
        self.length_max = length_max
        self.log.info("Packet length min {} max {}".format(
            length_min, length_max))

        self.send_wait = 1.0 / send_rate if send_rate else 0.0

        if not radio_id or radio_id < 0 or radio_id >= 255:
            raise ValueError("invalid or missing radio id")
        self.radio_id = radio_id
        self.log.info("Radio id is {}".format(self.radio_id))

        self.broadcast_addr = broadcast_addr
        self.broadcast_port = broadcast_port
        self.socket = socket.socket(
            socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setsockopt(
            socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.setsockopt(
            socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.socket.setsockopt(socket.SOL_IP, socket.IP_TTL, 254)
        self.socket.bind((self.broadcast_addr, self.broadcast_port))
        self.log.info("Bound to {}:{}".format(
            self.broadcast_addr, self.broadcast_port))
        self.destination_addr = destination_addr

        self.report = {'sent': 0, 'rcvd': {}, 'fail': 0}

        thread = threading.Thread(name="send", target=self._send)
        thread.daemon = True
        thread.start()

        thread = threading.Thread(name="recv", target=self._recv)
        thread.daemon = True
        thread.start()

        while True:
            time.sleep(1)
            report = self.report
            self.report = {'sent': 0, 'rcvd': {}, 'fail': 0}
            print("sent: {} rcvd: {} fail: {}".format(
                report['sent'], report['rcvd'], report['fail']))

    def _send(self):
        send_time = time.time()
        while True:
            length = random.randint(self.length_min, self.length_max)
            payload = ''.join([chr(int(random.random() * 256))
                               for _ in range(length - 5)])
            header = struct.pack('!Bl', self.radio_id, zlib.crc32(payload))
            curr_time = time.time()
            if send_time > curr_time:
                time.sleep(send_time - curr_time)
            try:
                self.socket.sendto(
                    header + payload, (self.destination_addr, self.broadcast_port))
                self.report['sent'] += 1
                send_time += self.send_wait
            except:
                self.log.info('send failed, waiting 1 second')
                send_time = time.time() + 1.0

    def _recv(self):
        while True:
            data, _ = self.socket.recvfrom(65535)
            if len(data) < 5:
                self.report['fail'] += 1
                continue
            radio_id, crc32 = struct.unpack('!Bl', data[:5])
            if crc32 != zlib.crc32(data[5:]):
                self.report['fail'] += 1
                continue
            self.report['rcvd'][radio_id] = self.report[
                'rcvd'].get(radio_id, 0) + 1


def get_ip_address(ifname):
    """Gets the ip address from the interface name."""
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
    except:
        return None
    finally:
        s.close()


def run():
    logging.basicConfig(level=logging.INFO)

    ip_addr = get_ip_address(IF_NAME)
    radio_id = int(ip_addr.split(".")[3]) if ip_addr else None

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-i', '--radio-id', help="radio id of this node",
        metavar="ID", type=int, default=radio_id)
    parser.add_argument(
        '-a', '--addr', help="broadcast address",
        metavar="ADDR", default="192.168.45." + str(radio_id))
    parser.add_argument(
        '-p', '--port', help="UDP port number",
        metavar="PORT", type=int, default=4712)
    parser.add_argument(
        '-r', '--rate', help="messages per seconds",
        metavar="RATE", type=float, default=None)
    parser.add_argument(
        '--min', help="minimum message length",
        metavar="LEN", type=int, default=5)
    parser.add_argument(
        '--max', help="maximum message length",
        metavar="LEN", type=int, default=1472)
    parser.add_argument(
        '-d', '--dst', help="destination address",
        metavar="ADDR", default="192.168.45.255")
    args = parser.parse_args()

    Generator(radio_id=args.radio_id, broadcast_addr=args.addr,
              broadcast_port=args.port, send_rate=args.rate,
              length_min=args.min, length_max=args.max,
              destination_addr=args.dst)


if __name__ == "__main__":
    run()
