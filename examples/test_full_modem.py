#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Full Modem
# Generated: Thu Sep 26 22:06:49 2019
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import marmote3
import math
import numpy as np
import random
import sip
import sys
from gnuradio import qtgui


class test_full_modem(gr.top_block, Qt.QWidget):

    def __init__(self, cpu_fifo=65536, num_channels=32, radio_id=-1, rx_subdev="A:0", samp_rate=50000000/4, tx_subdev="A:0"):
        gr.top_block.__init__(self, "Test Full Modem")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Test Full Modem")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "test_full_modem")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Parameters
        ##################################################
        self.cpu_fifo = cpu_fifo
        self.num_channels = num_channels
        self.radio_id = radio_id
        self.rx_subdev = rx_subdev
        self.samp_rate = samp_rate
        self.tx_subdev = tx_subdev

        ##################################################
        # Variables
        ##################################################
        self.max_data_len = max_data_len = 1506
        self.chirp_len = chirp_len = 128
        self.padding_len = padding_len = 8
        self.max_symb_len = max_symb_len = marmote3.get_mcscheme(0).get_symb_len(max_data_len)
        self.fbmc_fft_len = fbmc_fft_len = int(num_channels * 5 / 4)
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)
        self.tx_channel = tx_channel = 6
        self.subcarrier_map = subcarrier_map = range(fbmc_fft_len-num_channels/2, fbmc_fft_len) + range(0, num_channels/2)
        self.max_frame_len = max_frame_len = 40 + padding_len + chirp_len + padding_len + max_symb_len
        self.fbmc_tx_stride = fbmc_tx_stride = {16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[num_channels]
        self.dc_chirp = dc_chirp = chirp + np.ones(chirp_len) * 0.4
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 2, "tcp://127.0.0.1:7555")
        self.tx_power = tx_power = 24
        self.tx_gain = tx_gain = 24
        self.tx_device = tx_device = "addr=192.168.10.2"
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.spectrogram_fft = spectrogram_fft = 256
        self.rx_noise = rx_noise = 0.000
        self.rx_device = rx_device = "addr=192.168.20.2"
        self.rx_channel = rx_channel = tx_channel
        self.received_preamble = received_preamble = marmote3.get_received_signal8(dc_chirp, fbmc_fft_len, fbmc_tx_stride)
        self.postamble = postamble = True
        self.max_frame_time = max_frame_time = float(max_frame_len) / (samp_rate / fbmc_tx_stride)
        self.max_code_len = max_code_len = marmote3.get_mcscheme(0).get_code_len(max_data_len)
        self.gui_update = gui_update = 0.2
        self.freq_err = freq_err = 0.0
        self.freq = freq = 1e9
        self.frame_delay = frame_delay = 0
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.fbmc_rx_taps = fbmc_rx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "rx")
        self.fbmc_rx_stride = fbmc_rx_stride = fbmc_tx_stride/2
        self.delay = delay = 22
        self.chirp_fft_len = chirp_fft_len = 64
        self.active_channels = active_channels = [tx_channel, tx_channel+1] if True else random.sample(range(subcarriers), 1)

        ##################################################
        # Blocks
        ##################################################
        self._tx_channel_range = Range(0, num_channels-1, 1, 6, 200)
        self._tx_channel_win = RangeWidget(self._tx_channel_range, self.set_tx_channel, 'TX channel', "counter_slider", int)
        self.top_grid_layout.addWidget(self._tx_channel_win, 0, 2, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._tx_power_range = Range(-20, 40, 1, 24, 200)
        self._tx_power_win = RangeWidget(self._tx_power_range, self.set_tx_power, 'TX power plus gain in dB', "counter_slider", int)
        self.top_grid_layout.addWidget(self._tx_power_win, 0, 0, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._tx_gain_range = Range(0, 30, 1, 24, 200)
        self._tx_gain_win = RangeWidget(self._tx_gain_range, self.set_tx_gain, 'TX gain', "counter_slider", int)
        self.top_grid_layout.addWidget(self._tx_gain_win, 0, 1, 1, 1)
        for r in range(0, 1):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._rx_noise_range = Range(0.0, 1.0, 0.001, 0.000, 200)
        self._rx_noise_win = RangeWidget(self._rx_noise_range, self.set_rx_noise, 'RX noise', "counter_slider", float)
        self.top_grid_layout.addWidget(self._rx_noise_win, 3, 2, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._rx_channel_range = Range(0, num_channels-1, 1, tx_channel, 200)
        self._rx_channel_win = RangeWidget(self._rx_channel_range, self.set_rx_channel, 'RX channel', "counter_slider", int)
        self.top_grid_layout.addWidget(self._rx_channel_win, 3, 1, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._freq_err_range = Range(-20000, 20000, 1, 0.0, 200)
        self._freq_err_win = RangeWidget(self._freq_err_range, self.set_freq_err, 'Freq err', "counter_slider", float)
        self.top_grid_layout.addWidget(self._freq_err_win, 4, 1, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._freq_range = Range(0.5e9, 2.5e9, 0.1e9, 1e9, 200)
        self._freq_win = RangeWidget(self._freq_range, self.set_freq, 'freq', "counter_slider", float)
        self.top_grid_layout.addWidget(self._freq_win, 4, 2, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._frame_delay_range = Range(-100, 100, 1, 0, 200)
        self._frame_delay_win = RangeWidget(self._frame_delay_range, self.set_frame_delay, 'frame delay', "counter_slider", int)
        self.top_grid_layout.addWidget(self._frame_delay_win, 4, 0, 1, 1)
        for r in range(4, 5):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self._delay_range = Range(-100000, 100000, 1, 22, 200)
        self._delay_win = RangeWidget(self._delay_range, self.set_delay, 'sample delay', "counter_slider", int)
        self.top_grid_layout.addWidget(self._delay_win, 3, 0, 1, 1)
        for r in range(3, 4):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.z1_marmote3_modem_sender_0 = marmote3.modem_sender2(max_data_len, max_code_len, max_symb_len, 5000, 9, 0.35, 20e6, False, 4, radio_id, (), 2, True, False)
        self.z1_marmote3_modem_sender_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.spectrum_radar_detector = marmote3.radar_detector(spectrogram_fft, 0.05, 50.0, 10, 5000, False)
        self.spectrum_radar_detector.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.spectrum_fft_and_magnitude = marmote3.fft_and_magnitude(spectrogram_fft, spectrogram_fft / 2, True, (window.hann(spectrogram_fft)), True)
        self.qtgui_waterfall_sink_x_0 = qtgui.waterfall_sink_c(
        	1024*4, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	freq, #fc
        	samp_rate, #bw
        	"", #name
                1 #number of inputs
        )
        self.qtgui_waterfall_sink_x_0.set_update_time(gui_update)
        self.qtgui_waterfall_sink_x_0.enable_grid(False)
        self.qtgui_waterfall_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_waterfall_sink_x_0.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_waterfall_sink_x_0.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        colors = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_waterfall_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_waterfall_sink_x_0.set_color_map(i, colors[i])
            self.qtgui_waterfall_sink_x_0.set_line_alpha(i, alphas[i])

        self.qtgui_waterfall_sink_x_0.set_intensity_range(-110, -10)

        self._qtgui_waterfall_sink_x_0_win = sip.wrapinstance(self.qtgui_waterfall_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_waterfall_sink_x_0_win, 1, 1, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0_1 = qtgui.time_sink_f(
        	6 * chirp_len, #size
        	samp_rate/num_channels, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_1.set_update_time(gui_update)
        self.qtgui_time_sink_x_0_1.set_y_axis(0, 3)

        self.qtgui_time_sink_x_0_1.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_1.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_1.set_trigger_mode(qtgui.TRIG_MODE_TAG, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "packet_len")
        self.qtgui_time_sink_x_0_1.enable_autoscale(True)
        self.qtgui_time_sink_x_0_1.enable_grid(False)
        self.qtgui_time_sink_x_0_1.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_1.enable_control_panel(False)
        self.qtgui_time_sink_x_0_1.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_1.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_1.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_1.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_1.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_1.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_1.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_1.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_1.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_1_win = sip.wrapinstance(self.qtgui_time_sink_x_0_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_1_win, 2, 1, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_time_sink_x_0_0 = qtgui.time_sink_f(
        	max_frame_len, #size
        	samp_rate/num_channels, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0_0.set_update_time(gui_update)
        self.qtgui_time_sink_x_0_0.set_y_axis(0, 20)

        self.qtgui_time_sink_x_0_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0_0.enable_grid(False)
        self.qtgui_time_sink_x_0_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_0_win, 2, 0, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_number_sink_0_1 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            1
        )
        self.qtgui_number_sink_0_1.set_update_time(gui_update)
        self.qtgui_number_sink_0_1.set_title("Receive RMS")

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0_1.set_min(i, -80)
            self.qtgui_number_sink_0_1.set_max(i, 10)
            self.qtgui_number_sink_0_1.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0_1.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0_1.set_label(i, labels[i])
            self.qtgui_number_sink_0_1.set_unit(i, units[i])
            self.qtgui_number_sink_0_1.set_factor(i, factor[i])

        self.qtgui_number_sink_0_1.enable_autoscale(False)
        self._qtgui_number_sink_0_1_win = sip.wrapinstance(self.qtgui_number_sink_0_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_1_win, 5, 2, 1, 1)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_number_sink_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            1
        )
        self.qtgui_number_sink_0.set_update_time(gui_update)
        self.qtgui_number_sink_0.set_title("Transmit RMS")

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0.set_min(i, -80)
            self.qtgui_number_sink_0.set_max(i, 10)
            self.qtgui_number_sink_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0.set_label(i, labels[i])
            self.qtgui_number_sink_0.set_unit(i, units[i])
            self.qtgui_number_sink_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0.enable_autoscale(False)
        self._qtgui_number_sink_0_win = sip.wrapinstance(self.qtgui_number_sink_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_win, 5, 1, 1, 1)
        for r in range(5, 6):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(1, 2):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_c(
        	1024*4, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	freq, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0.set_update_time(gui_update)
        self.qtgui_freq_sink_x_0.set_y_axis(-120, -10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(0.05)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_0.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_0.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_0_win, 1, 0, 1, 1)
        for r in range(1, 2):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(0, 1):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.qtgui_const_sink_x_0 = qtgui.const_sink_c(
        	max_symb_len, #size
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_const_sink_x_0.set_update_time(gui_update)
        self.qtgui_const_sink_x_0.set_y_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_x_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, "")
        self.qtgui_const_sink_x_0.enable_autoscale(False)
        self.qtgui_const_sink_x_0.enable_grid(False)
        self.qtgui_const_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_const_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "red", "red", "red",
                  "red", "red", "red", "red", "red"]
        styles = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_const_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_const_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_const_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_const_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_const_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_const_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_const_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_const_sink_x_0_win = sip.wrapinstance(self.qtgui_const_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_const_sink_x_0_win, 2, 2, 1, 1)
        for r in range(2, 3):
            self.top_grid_layout.setRowStretch(r, 1)
        for c in range(2, 3):
            self.top_grid_layout.setColumnStretch(c, 1)
        self.marmote3_vector_peak_probe_1 = marmote3.vector_peak_probe(fbmc_fft_len, 2, 15, -30, 2.0)
        self.marmote3_vector_peak_probe_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_vector_peak_probe_0 = marmote3.vector_peak_probe(fbmc_fft_len, 1, 30, -60.0, 2.0)
        self.marmote3_vector_peak_probe_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_transpose_vxx_0_0_0 = marmote3.transpose_vxx(gr.sizeof_float, chirp_fft_len, num_channels)
        self.marmote3_transpose_vxx_0_0 = marmote3.transpose_vxx(gr.sizeof_gr_complex, num_channels, chirp_fft_len)
        self.marmote3_test_message_source_0 = marmote3.test_message_source(130, 130, 4, 200000, 200, 1, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(4)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_subcarrier_serializer2_0 = marmote3.subcarrier_serializer2(num_channels, 2 * chirp_len / chirp_fft_len, 10.0, 2 * (padding_len + 40 + padding_len + chirp_len) + 3, 2 * (padding_len + 40 + padding_len) + 1, postamble, 2 * (padding_len + 40 + padding_len + chirp_len + padding_len), 2 * (padding_len + max_frame_len + padding_len), ([rx_channel]))
        self.marmote3_subcarrier_serializer2_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_subcarrier_remapper_1 = marmote3.subcarrier_remapper(fbmc_fft_len, (subcarrier_map), 0)
        self.marmote3_subcarrier_remapper_0 = marmote3.subcarrier_remapper(num_channels, (subcarrier_inv), 32)
        self.marmote3_subcarrier_allocator_0 = marmote3.subcarrier_allocator(num_channels, (active_channels), (), 3, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), (), getattr(self, 'z1_marmote3_modem_sender_0') if 'z1_marmote3_modem_sender_0' else marmote3.modem_sender2_sptr(), True)
        self.marmote3_subcarrier_allocator_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_spectrum_inverter_0 = marmote3.spectrum_inverter(False)
        self.marmote3_spectrum_inverter_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        (self.marmote3_spectrum_inverter_0).set_min_output_buffer(32768)
        self.marmote3_polyphase_synt_filter_ccf_0 = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, (fbmc_tx_taps), fbmc_tx_stride or fbmc_fft_len)
        self.marmote3_polyphase_synt_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        (self.marmote3_polyphase_synt_filter_ccf_0).set_min_output_buffer(8192)
        self.marmote3_polyphase_rotator_vcc_0_0 = marmote3.polyphase_rotator_vcc(fbmc_fft_len, (subcarrier_map), -fbmc_rx_stride)
        self.marmote3_polyphase_rotator_vcc_0 = marmote3.polyphase_rotator_vcc(fbmc_fft_len, (subcarrier_map), fbmc_tx_stride)
        self.marmote3_polyphase_chan_filter_ccf_0 = marmote3.polyphase_chan_filter_ccf(fbmc_fft_len, (fbmc_rx_taps), fbmc_rx_stride or fbmc_fft_len)
        self.marmote3_polyphase_chan_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_periodic_multiply_const_cc_1_0 = marmote3.periodic_multiply_const_cc(1, (np.exp(-1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len))))
        self.marmote3_periodic_multiply_const_cc_1 = marmote3.periodic_multiply_const_cc(1, (np.exp(1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len))))
        self.marmote3_periodic_multiply_const_cc_0 = marmote3.periodic_multiply_const_cc(num_channels, (np.conj(chirp)))
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(marmote3.MOD_MCS or 1, max_symb_len)
        self.marmote3_packet_symb_demod_0 = marmote3.packet_symb_demod(marmote3.MOD_MCS or 1, 36.0, max_code_len * 8)
        self.marmote3_packet_ra_encoder_0 = marmote3.packet_ra_encoder(-1 if True else max_data_len, max_code_len)
        self.marmote3_packet_ra_decoder_0 = marmote3.packet_ra_decoder(max_data_len, -1 if True else 0, 2, 8)
        self.marmote3_packet_preamb_insert_0 = marmote3.packet_preamb_insert((np.concatenate([dc_chirp[-padding_len:],dc_chirp,dc_chirp[:padding_len]])), True, tx_power - tx_gain, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_frame_len)
        self.marmote3_packet_preamb_freqcorr2_0 = marmote3.packet_preamb_freqcorr2(2 * (padding_len + 40 + padding_len), 2 * chirp_len, postamble, 16, 0.25, 2 * (padding_len + max_frame_len + padding_len))
        self.marmote3_packet_preamb_equalizer_0 = marmote3.packet_preamb_equalizer((received_preamble), 2, padding_len, marmote3.HDR_SRC_MCS_LEN_CRC, postamble, max_symb_len, 0.2, False, False)
        self.marmote3_packet_preamb_equalizer_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_packet_freq_corrector_0 = marmote3.packet_freq_corrector(marmote3.MOD_MCS or 1, 64, postamble, max_symb_len)
        self.marmote3_packet_flow_splitter_0 = marmote3.packet_flow_splitter(gr.sizeof_gr_complex * 1, 1, 2 * (padding_len + max_frame_len + padding_len))
        self.marmote3_packet_flow_splitter_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_packet_flow_merger_0 = marmote3.packet_flow_merger(gr.sizeof_char * 1, 1, max_data_len)
        self.marmote3_packet_flow_merger_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_packet_ampl_corrector_0 = marmote3.packet_ampl_corrector(512, max_symb_len)
        self.marmote3_modem_receiver_0 = marmote3.modem_receiver(4, getattr(self, 'z1_marmote3_modem_sender_0') if 'z1_marmote3_modem_sender_0' else marmote3.modem_sender2_sptr(), True)
        self.marmote3_modem_receiver_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_control_0 = marmote3.modem_control('freq', 'samp_rate', '', 'tx_gain', 'tx_power', '', '')
        self.marmote3_modem_control_0.set_center_freq(freq)
        self.marmote3_modem_control_0.set_samp_rate(samp_rate)
        self.marmote3_modem_control_0.set_rx_gain()
        self.marmote3_modem_control_0.set_tx_gain(tx_gain)
        self.marmote3_modem_control_0.set_tx_power(tx_power)
        self.marmote3_modem_control_0.set_invert_spectrum()
        self.marmote3_modem_control_0.set_enable_recording()
        self.marmote3_modem_control_0.set_num_channels(num_channels)
        self.marmote3_modem_control_0.set_num_subcarriers(fbmc_fft_len)
        self._modem_setter = marmote3.ModemSetter(self)
        self.marmote3_modem_control_0.set_modem_setter(self._modem_setter)
        self.marmote3_modem_control_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_0 = marmote3.header_decompressor(4, True)
        self.marmote3_header_decompressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(4, True)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_chirp_frame_timing_0 = marmote3.chirp_frame_timing(chirp_len, chirp_fft_len, num_channels, 0.0)
        self.marmote3_block_sum_vxx_0 = marmote3.block_sum_vxx(1 * num_channels, 1, 2)
        self.fft_vxx_1_0 = fft.fft_vcc(chirp_fft_len, True, (), False, 1)
        self.fft_vxx_0_0 = fft.fft_vcc(fbmc_fft_len, True, (), False, 1)
        self.fft_vxx_0 = fft.fft_vcc(fbmc_fft_len, False, (), False, 1)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_float*1, num_channels)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_rms_xx_0_1 = blocks.rms_cf(0.00001)
        self.blocks_rms_xx_0 = blocks.rms_cf(0.0001)
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_float*1, chirp_len / chirp_fft_len)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(20, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(20, 1, 0)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_keep_one_in_n_0 = blocks.keep_one_in_n(gr.sizeof_gr_complex*num_channels, 2)
        self.blocks_keep_m_in_n_1 = blocks.keep_m_in_n(gr.sizeof_float, 1, num_channels, 0)
        self.blocks_delay_2_0 = blocks.delay(gr.sizeof_gr_complex*num_channels, frame_delay)
        self.blocks_delay_1 = blocks.delay(gr.sizeof_float*1, num_channels - rx_channel)
        self.blocks_delay_0 = blocks.delay(gr.sizeof_gr_complex*1, delay)
        self.blocks_copy_0 = blocks.copy(gr.sizeof_gr_complex*1)
        self.blocks_copy_0.set_enabled(True)
        self.blocks_complex_to_mag_squared_0_1 = blocks.complex_to_mag_squared(chirp_fft_len)
        self.blocks_complex_to_mag_squared_0_0 = blocks.complex_to_mag_squared(1)
        self.blocks_add_xx_2 = blocks.add_vcc(1)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, freq_err, 1.0, 0)
        self.analog_fastnoise_source_x_0 = analog.fastnoise_source_c(analog.GR_GAUSSIAN, rx_noise, 0, 8192)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.z1_marmote3_modem_sender_0, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))
        self.msg_connect((self.marmote3_modem_receiver_0, 'out'), (self.marmote3_header_decompressor_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))
        self.connect((self.analog_fastnoise_source_x_0, 0), (self.blocks_add_xx_2, 0))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.marmote3_spectrum_inverter_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_0, 0), (self.qtgui_time_sink_x_0_1, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_1, 0), (self.marmote3_chirp_frame_timing_0, 0))
        self.connect((self.blocks_copy_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_delay_0, 0), (self.blocks_copy_0, 0))
        self.connect((self.blocks_delay_1, 0), (self.blocks_keep_m_in_n_1, 0))
        self.connect((self.blocks_delay_2_0, 0), (self.marmote3_subcarrier_serializer2_0, 0))
        self.connect((self.blocks_keep_m_in_n_1, 0), (self.blocks_repeat_0, 0))
        self.connect((self.blocks_keep_one_in_n_0, 0), (self.marmote3_periodic_multiply_const_cc_0, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_add_xx_2, 1))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_number_sink_0, 0))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.qtgui_number_sink_0_1, 0))
        self.connect((self.blocks_repeat_0, 0), (self.qtgui_time_sink_x_0_0, 0))
        self.connect((self.blocks_rms_xx_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_rms_xx_0_1, 0), (self.blocks_nlog10_ff_0_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_delay_0, 0))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.blocks_delay_1, 0))
        self.connect((self.fft_vxx_0, 0), (self.marmote3_polyphase_synt_filter_ccf_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.marmote3_subcarrier_remapper_1, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.marmote3_vector_peak_probe_0, 0))
        self.connect((self.fft_vxx_1_0, 0), (self.blocks_complex_to_mag_squared_0_1, 0))
        self.connect((self.marmote3_block_sum_vxx_0, 0), (self.blocks_vector_to_stream_0, 0))
        self.connect((self.marmote3_block_sum_vxx_0, 0), (self.marmote3_subcarrier_serializer2_0, 1))
        self.connect((self.marmote3_chirp_frame_timing_0, 0), (self.marmote3_transpose_vxx_0_0_0, 0))
        self.connect((self.marmote3_packet_ampl_corrector_0, 0), (self.marmote3_packet_freq_corrector_0, 0))
        self.connect((self.marmote3_packet_flow_merger_0, 0), (self.marmote3_modem_receiver_0, 0))
        self.connect((self.marmote3_packet_flow_splitter_0, 0), (self.marmote3_packet_preamb_freqcorr2_0, 0))
        self.connect((self.marmote3_packet_freq_corrector_0, 0), (self.marmote3_packet_symb_demod_0, 0))
        self.connect((self.marmote3_packet_freq_corrector_0, 0), (self.qtgui_const_sink_x_0, 0))
        self.connect((self.marmote3_packet_preamb_equalizer_0, 0), (self.marmote3_packet_ampl_corrector_0, 0))
        self.connect((self.marmote3_packet_preamb_freqcorr2_0, 0), (self.marmote3_packet_preamb_equalizer_0, 0))
        self.connect((self.marmote3_packet_preamb_insert_0, 0), (self.marmote3_subcarrier_allocator_0, 0))
        self.connect((self.marmote3_packet_ra_decoder_0, 0), (self.marmote3_packet_flow_merger_0, 0))
        self.connect((self.marmote3_packet_ra_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_demod_0, 0), (self.marmote3_packet_ra_decoder_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.marmote3_packet_preamb_insert_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_0, 0), (self.marmote3_transpose_vxx_0_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_1, 0), (self.blocks_throttle_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_1_0, 0), (self.marmote3_polyphase_chan_filter_ccf_0, 0))
        self.connect((self.marmote3_polyphase_chan_filter_ccf_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.marmote3_polyphase_rotator_vcc_0, 0), (self.marmote3_subcarrier_remapper_0, 0))
        self.connect((self.marmote3_polyphase_rotator_vcc_0_0, 0), (self.blocks_delay_2_0, 0))
        self.connect((self.marmote3_polyphase_rotator_vcc_0_0, 0), (self.blocks_keep_one_in_n_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.blocks_rms_xx_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.marmote3_periodic_multiply_const_cc_1, 0))
        self.connect((self.marmote3_spectrum_inverter_0, 0), (self.blocks_rms_xx_0_1, 0))
        self.connect((self.marmote3_spectrum_inverter_0, 0), (self.marmote3_periodic_multiply_const_cc_1_0, 0))
        self.connect((self.marmote3_spectrum_inverter_0, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.marmote3_spectrum_inverter_0, 0), (self.qtgui_waterfall_sink_x_0, 0))
        self.connect((self.marmote3_spectrum_inverter_0, 0), (self.spectrum_fft_and_magnitude, 0))
        self.connect((self.marmote3_subcarrier_allocator_0, 0), (self.marmote3_polyphase_rotator_vcc_0, 0))
        self.connect((self.marmote3_subcarrier_remapper_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.marmote3_subcarrier_remapper_0, 0), (self.marmote3_vector_peak_probe_1, 0))
        self.connect((self.marmote3_subcarrier_remapper_1, 0), (self.marmote3_polyphase_rotator_vcc_0_0, 0))
        self.connect((self.marmote3_subcarrier_serializer2_0, 0), (self.blocks_complex_to_mag_squared_0_0, 0))
        self.connect((self.marmote3_subcarrier_serializer2_0, 0), (self.marmote3_packet_flow_splitter_0, 0))
        self.connect((self.marmote3_transpose_vxx_0_0, 0), (self.fft_vxx_1_0, 0))
        self.connect((self.marmote3_transpose_vxx_0_0_0, 0), (self.marmote3_block_sum_vxx_0, 0))
        self.connect((self.spectrum_fft_and_magnitude, 0), (self.spectrum_radar_detector, 0))
        self.connect((self.z1_marmote3_modem_sender_0, 0), (self.marmote3_packet_ra_encoder_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "test_full_modem")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_cpu_fifo(self):
        return self.cpu_fifo

    def set_cpu_fifo(self, cpu_fifo):
        self.cpu_fifo = cpu_fifo

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_subcarrier_map(range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len) + range(0, self.num_channels/2))
        self.set_fbmc_tx_stride({16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[self.num_channels])
        self.set_fbmc_fft_len(int(self.num_channels * 5 / 4))
        self.qtgui_time_sink_x_0_1.set_samp_rate(self.samp_rate/self.num_channels)
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate/self.num_channels)
        self.marmote3_modem_control_0.set_num_channels(self.num_channels)
        self.blocks_keep_m_in_n_1.set_n(self.num_channels)
        self.blocks_delay_1.set_dly(self.num_channels - self.rx_channel)

    def get_radio_id(self):
        return self.radio_id

    def set_radio_id(self, radio_id):
        self.radio_id = radio_id

    def get_rx_subdev(self):
        return self.rx_subdev

    def set_rx_subdev(self, rx_subdev):
        self.rx_subdev = rx_subdev

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.qtgui_time_sink_x_0_1.set_samp_rate(self.samp_rate/self.num_channels)
        self.qtgui_time_sink_x_0_0.set_samp_rate(self.samp_rate/self.num_channels)
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))
        self.marmote3_modem_control_0.set_samp_rate(self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_tx_subdev(self):
        return self.tx_subdev

    def set_tx_subdev(self, tx_subdev):
        self.tx_subdev = tx_subdev

    def get_max_data_len(self):
        return self.max_data_len

    def set_max_data_len(self, max_data_len):
        self.max_data_len = max_data_len
        self.set_max_symb_len(marmote3.get_mcscheme(0).get_symb_len(self.max_data_len))
        self.set_max_code_len(marmote3.get_mcscheme(0).get_code_len(self.max_data_len))

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.4)
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))
        self.blocks_repeat_0.set_interpolation(self.chirp_len / self.chirp_fft_len)

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_max_symb_len(self):
        return self.max_symb_len

    def set_max_symb_len(self, max_symb_len):
        self.max_symb_len = max_symb_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_subcarrier_map(range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len) + range(0, self.num_channels/2))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.set_received_preamble(marmote3.get_received_signal8(self.dc_chirp, self.fbmc_fft_len, self.fbmc_tx_stride))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.marmote3_periodic_multiply_const_cc_1_0.set_vals((np.exp(-1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len))))
        self.marmote3_periodic_multiply_const_cc_1.set_vals((np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len))))
        self.marmote3_modem_control_0.set_num_subcarriers(self.fbmc_fft_len)

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.4)
        self.marmote3_periodic_multiply_const_cc_0.set_vals((np.conj(self.chirp)))

    def get_tx_channel(self):
        return self.tx_channel

    def set_tx_channel(self, tx_channel):
        self.tx_channel = tx_channel
        self.set_rx_channel(self.tx_channel)
        self.set_active_channels([self.tx_channel, self.tx_channel+1] if True else random.sample(range(subcarriers), 1))

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_max_frame_len(self):
        return self.max_frame_len

    def set_max_frame_len(self, max_frame_len):
        self.max_frame_len = max_frame_len
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_received_preamble(marmote3.get_received_signal8(self.dc_chirp, self.fbmc_fft_len, self.fbmc_tx_stride))
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.set_fbmc_rx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "rx"))
        self.set_fbmc_rx_stride(self.fbmc_tx_stride/2)
        self.set_max_frame_time(float(self.max_frame_len) / (self.samp_rate / self.fbmc_tx_stride))

    def get_dc_chirp(self):
        return self.dc_chirp

    def set_dc_chirp(self, dc_chirp):
        self.dc_chirp = dc_chirp
        self.set_received_preamble(marmote3.get_received_signal8(self.dc_chirp, self.fbmc_fft_len, self.fbmc_tx_stride))

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_tx_power(self):
        return self.tx_power

    def set_tx_power(self, tx_power):
        self.tx_power = tx_power
        self.marmote3_packet_preamb_insert_0.set_power(self.tx_power - self.tx_gain)
        self.marmote3_modem_control_0.set_tx_power(self.tx_power)

    def get_tx_gain(self):
        return self.tx_gain

    def set_tx_gain(self, tx_gain):
        self.tx_gain = tx_gain
        self.marmote3_packet_preamb_insert_0.set_power(self.tx_power - self.tx_gain)
        self.marmote3_modem_control_0.set_tx_gain(self.tx_gain)

    def get_tx_device(self):
        return self.tx_device

    def set_tx_device(self, tx_device):
        self.tx_device = tx_device

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_spectrogram_fft(self):
        return self.spectrogram_fft

    def set_spectrogram_fft(self, spectrogram_fft):
        self.spectrogram_fft = spectrogram_fft

    def get_rx_noise(self):
        return self.rx_noise

    def set_rx_noise(self, rx_noise):
        self.rx_noise = rx_noise
        self.analog_fastnoise_source_x_0.set_amplitude(self.rx_noise)

    def get_rx_device(self):
        return self.rx_device

    def set_rx_device(self, rx_device):
        self.rx_device = rx_device

    def get_rx_channel(self):
        return self.rx_channel

    def set_rx_channel(self, rx_channel):
        self.rx_channel = rx_channel
        self.marmote3_subcarrier_serializer2_0.set_enabled(([self.rx_channel]))
        self.blocks_delay_1.set_dly(self.num_channels - self.rx_channel)

    def get_received_preamble(self):
        return self.received_preamble

    def set_received_preamble(self, received_preamble):
        self.received_preamble = received_preamble

    def get_postamble(self):
        return self.postamble

    def set_postamble(self, postamble):
        self.postamble = postamble

    def get_max_frame_time(self):
        return self.max_frame_time

    def set_max_frame_time(self, max_frame_time):
        self.max_frame_time = max_frame_time

    def get_max_code_len(self):
        return self.max_code_len

    def set_max_code_len(self, max_code_len):
        self.max_code_len = max_code_len

    def get_gui_update(self):
        return self.gui_update

    def set_gui_update(self, gui_update):
        self.gui_update = gui_update
        self.qtgui_waterfall_sink_x_0.set_update_time(self.gui_update)
        self.qtgui_time_sink_x_0_1.set_update_time(self.gui_update)
        self.qtgui_time_sink_x_0_0.set_update_time(self.gui_update)
        self.qtgui_number_sink_0_1.set_update_time(self.gui_update)
        self.qtgui_number_sink_0.set_update_time(self.gui_update)
        self.qtgui_freq_sink_x_0.set_update_time(self.gui_update)
        self.qtgui_const_sink_x_0.set_update_time(self.gui_update)

    def get_freq_err(self):
        return self.freq_err

    def set_freq_err(self, freq_err):
        self.freq_err = freq_err
        self.analog_sig_source_x_0.set_frequency(self.freq_err)

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.qtgui_waterfall_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(self.freq, self.samp_rate)
        self.marmote3_modem_control_0.set_center_freq(self.freq)

    def get_frame_delay(self):
        return self.frame_delay

    def set_frame_delay(self, frame_delay):
        self.frame_delay = frame_delay
        self.blocks_delay_2_0.set_dly(self.frame_delay)

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_fbmc_rx_taps(self):
        return self.fbmc_rx_taps

    def set_fbmc_rx_taps(self, fbmc_rx_taps):
        self.fbmc_rx_taps = fbmc_rx_taps

    def get_fbmc_rx_stride(self):
        return self.fbmc_rx_stride

    def set_fbmc_rx_stride(self, fbmc_rx_stride):
        self.fbmc_rx_stride = fbmc_rx_stride

    def get_delay(self):
        return self.delay

    def set_delay(self, delay):
        self.delay = delay
        self.blocks_delay_0.set_dly(self.delay)

    def get_chirp_fft_len(self):
        return self.chirp_fft_len

    def set_chirp_fft_len(self, chirp_fft_len):
        self.chirp_fft_len = chirp_fft_len
        self.blocks_repeat_0.set_interpolation(self.chirp_len / self.chirp_fft_len)

    def get_active_channels(self):
        return self.active_channels

    def set_active_channels(self, active_channels):
        self.active_channels = active_channels
        self.marmote3_subcarrier_allocator_0.set_active_channels((self.active_channels))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--cpu-fifo", dest="cpu_fifo", type="intx", default=65536,
        help="Set CPU output buffer size in bytes [default=%default]")
    parser.add_option(
        "-n", "--num-channels", dest="num_channels", type="intx", default=32,
        help="Set number of channels [default=%default]")
    parser.add_option(
        "", "--radio-id", dest="radio_id", type="intx", default=-1,
        help="Set radio id [default=%default]")
    parser.add_option(
        "", "--rx-subdev", dest="rx_subdev", type="string", default="A:0",
        help="Set rx_subdev [default=%default]")
    parser.add_option(
        "-r", "--samp-rate", dest="samp_rate", type="long", default=50000000/4,
        help="Set sample rate [default=%default]")
    parser.add_option(
        "", "--tx-subdev", dest="tx_subdev", type="string", default="A:0",
        help="Set tx_subdev [default=%default]")
    return parser


def main(top_block_cls=test_full_modem, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(cpu_fifo=options.cpu_fifo, num_channels=options.num_channels, radio_id=options.radio_id, rx_subdev=options.rx_subdev, samp_rate=options.samp_rate, tx_subdev=options.tx_subdev)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
