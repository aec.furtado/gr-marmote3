#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Peak Probe Speed
# Generated: Thu Jul  4 22:50:59 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3
import numpy


class test_peak_probe_speed(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Peak Probe Speed")

        ##################################################
        # Variables
        ##################################################
        self.nfft = nfft = 20

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_vector_peak_probe_0 = marmote3.vector_peak_probe(20, 0, 0, 0.0, 0.0)
        self.marmote3_vector_peak_probe_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_peak_probe_0 = marmote3.peak_probe()
        self.marmote3_peak_probe_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, nfft)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 1000.0, 0.15)
        self.blocks_message_debug_0 = blocks.message_debug()
        self.blocks_interleaved_short_to_complex_0 = blocks.interleaved_short_to_complex(False, False)
        self.analog_random_source_x_0 = blocks.vector_source_s(map(int, numpy.random.randint(0, 256, 10000)), True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.blocks_message_debug_0, 'print'))
        self.connect((self.analog_random_source_x_0, 0), (self.blocks_interleaved_short_to_complex_0, 0))
        self.connect((self.blocks_interleaved_short_to_complex_0, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.blocks_interleaved_short_to_complex_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_interleaved_short_to_complex_0, 0), (self.marmote3_peak_probe_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.marmote3_vector_peak_probe_0, 0))

    def get_nfft(self):
        return self.nfft

    def set_nfft(self, nfft):
        self.nfft = nfft


def main(top_block_cls=test_peak_probe_speed, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
