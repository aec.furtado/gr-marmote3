#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Polyphase Speed
# Generated: Sun Nov 25 00:53:35 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3


class test_polyphase_speed(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Polyphase Speed")

        ##################################################
        # Variables
        ##################################################
        self.tx_stride = tx_stride = 28
        self.num_channels = num_channels = 25
        self.prototype_taps = prototype_taps = marmote3.get_fbmc_taps8(num_channels, tx_stride, "tx")
        self.samp_rate = samp_rate = 100000000
        self.rx_stride = rx_stride = tx_stride/2
        self.prototype_len = prototype_len = len(prototype_taps)

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_polyphase_synt_filter_ccf_0 = marmote3.polyphase_synt_filter_ccf(num_channels, (prototype_taps), tx_stride or num_channels)
        self.marmote3_polyphase_chan_filter_ccf_0 = marmote3.polyphase_chan_filter_ccf(num_channels, (prototype_taps), rx_stride or num_channels)
        self.fft_vxx_0_0 = fft.fft_vcc(num_channels, True, (window.rectangular(num_channels)), False, 1)
        self.fft_vxx_0 = fft.fft_vcc(num_channels, False, (window.rectangular(num_channels)), False, 1)
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, num_channels)
        self.blocks_vector_source_x_0_0 = blocks.vector_source_c(range(1024), True, 1, [])
        self.blocks_vector_source_x_0 = blocks.vector_source_c(range(1024), True, 1, [])
        self.blocks_throttle_0_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_gr_complex*1, num_channels)
        self.blocks_probe_rate_0_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 500.0, 0.15)
        self.blocks_probe_rate_0 = blocks.probe_rate(gr.sizeof_gr_complex*1, 500.0, 0.15)
        self.blocks_multiply_xx_0_0 = blocks.multiply_vcc(1)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self.blocks_message_debug_0_0 = blocks.message_debug()
        self.blocks_message_debug_0 = blocks.message_debug()
        self.analog_sig_source_x_0_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, -samp_rate / (2 * num_channels), 1, 0)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate, analog.GR_COS_WAVE, samp_rate / (2 * num_channels), 1, 0)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0, 'rate'), (self.blocks_message_debug_0, 'print'))
        self.msg_connect((self.blocks_probe_rate_0_0, 'rate'), (self.blocks_message_debug_0_0, 'print'))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.analog_sig_source_x_0_0, 0), (self.blocks_multiply_xx_0_0, 1))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_multiply_xx_0_0, 0), (self.marmote3_polyphase_chan_filter_ccf_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.fft_vxx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_probe_rate_0, 0))
        self.connect((self.blocks_throttle_0_0, 0), (self.blocks_multiply_xx_0_0, 0))
        self.connect((self.blocks_vector_source_x_0, 0), (self.blocks_throttle_0_0, 0))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.blocks_probe_rate_0_0, 0))
        self.connect((self.fft_vxx_0, 0), (self.marmote3_polyphase_synt_filter_ccf_0, 0))
        self.connect((self.fft_vxx_0_0, 0), (self.blocks_vector_to_stream_0_0, 0))
        self.connect((self.marmote3_polyphase_chan_filter_ccf_0, 0), (self.fft_vxx_0_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.blocks_multiply_xx_0, 0))

    def get_tx_stride(self):
        return self.tx_stride

    def set_tx_stride(self, tx_stride):
        self.tx_stride = tx_stride
        self.set_rx_stride(self.tx_stride/2)
        self.set_prototype_taps(marmote3.get_fbmc_taps8(self.num_channels, self.tx_stride, "tx"))

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_prototype_taps(marmote3.get_fbmc_taps8(self.num_channels, self.tx_stride, "tx"))
        self.analog_sig_source_x_0_0.set_frequency(-self.samp_rate / (2 * self.num_channels))
        self.analog_sig_source_x_0.set_frequency(self.samp_rate / (2 * self.num_channels))

    def get_prototype_taps(self):
        return self.prototype_taps

    def set_prototype_taps(self, prototype_taps):
        self.prototype_taps = prototype_taps
        self.set_prototype_len(len(self.prototype_taps))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0_0.set_sample_rate(self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)
        self.analog_sig_source_x_0_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0_0.set_frequency(-self.samp_rate / (2 * self.num_channels))
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0.set_frequency(self.samp_rate / (2 * self.num_channels))

    def get_rx_stride(self):
        return self.rx_stride

    def set_rx_stride(self, rx_stride):
        self.rx_stride = rx_stride

    def get_prototype_len(self):
        return self.prototype_len

    def set_prototype_len(self, prototype_len):
        self.prototype_len = prototype_len


def main(top_block_cls=test_polyphase_speed, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
