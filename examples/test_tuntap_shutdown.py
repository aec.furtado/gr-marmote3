#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Tuntap Shutdown
# Generated: Wed Aug 15 13:19:52 2018
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3


class test_tuntap_shutdown(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Tuntap Shutdown")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 32000

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(1)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_fixed_tuntap_pdu_0 = marmote3.fixed_tuntap_pdu('tap0', 1500, False)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_fixed_tuntap_pdu_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate


def main(top_block_cls=test_tuntap_shutdown, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
