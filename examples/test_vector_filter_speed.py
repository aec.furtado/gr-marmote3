#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Vector Filter Speed
# Generated: Wed Sep 26 22:12:25 2018
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3


class test_vector_filter_speed(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Vector Filter Speed")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 80000000
        self.num_channels = num_channels = 16

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_vector_fir_filter_xxx_0 = marmote3.vector_fir_filter_fff(num_channels, (range(40)))
        self.blocks_vector_to_stream_0_0 = blocks.vector_to_stream(gr.sizeof_float*1, num_channels)
        self.blocks_vector_source_x_0_0 = blocks.vector_source_f(range(1024), True, 1, [])
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_float*1, samp_rate,True)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_float*1, num_channels)
        self.blocks_probe_rate_0_0 = blocks.probe_rate(gr.sizeof_float*1, 500.0, 0.15)
        self.blocks_message_debug_0_0 = blocks.message_debug()



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.blocks_probe_rate_0_0, 'rate'), (self.blocks_message_debug_0_0, 'print'))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.marmote3_vector_fir_filter_xxx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_vector_source_x_0_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.blocks_vector_to_stream_0_0, 0), (self.blocks_probe_rate_0_0, 0))
        self.connect((self.marmote3_vector_fir_filter_xxx_0, 0), (self.blocks_vector_to_stream_0_0, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels


def main(top_block_cls=test_vector_filter_speed, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
