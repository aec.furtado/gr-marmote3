/* -*- c++ -*- */
/*
 * Copyright 2019 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FFT_AND_MAGNITUDE_H
#define INCLUDED_MARMOTE3_FFT_AND_MAGNITUDE_H

#include <gnuradio/block.h>
#include <marmote3/api.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API fft_and_magnitude : virtual public gr::block {
public:
  typedef boost::shared_ptr<fft_and_magnitude> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::fft_and_magnitude.
   *
   * To avoid accidental use of raw pointers, marmote3::fft_and_magnitude's
   * constructor is in a private implementation
   * class. marmote3::fft_and_magnitude::make is the public interface for
   * creating new instances.
   */
  static sptr make(int fft_size, int fft_stride, bool forward,
                   const std::vector<float> &window, bool shift);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_FFT_AND_MAGNITUDE_H */
