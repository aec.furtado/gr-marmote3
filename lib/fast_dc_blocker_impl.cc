/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fast_dc_blocker_impl.h"
#include "xmmintrin.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

fast_dc_blocker::sptr fast_dc_blocker::make(int length) {
  return gnuradio::get_initial_sptr(new fast_dc_blocker_impl(length));
}

fast_dc_blocker_impl::fast_dc_blocker_impl(int length)
    : gr::sync_block("fast_dc_blocker",
                     gr::io_signature::make(1, 1, sizeof(gr_complex)),
                     gr::io_signature::make(1, 1, sizeof(gr_complex))),
      length(length), scale(1.0f / ((length + 1) * (length + 1))) {
  if (this->length <= 0)
    throw std::out_of_range("fast_dc_blocker: length must be positive");

  pos = 0;
  sum1 = sum2 = gr_complex(0.0f, 0.0f);
  delay = new gr_complex[2 * length];
  for (int i = 0; i < 2 * length; i++)
    delay[i] = sum1;
}

fast_dc_blocker_impl::~fast_dc_blocker_impl() {
  delete[] delay;
  delay = NULL;
}

int fast_dc_blocker_impl::work(int noutput_items,
                               gr_vector_const_void_star &input_items,
                               gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  for (int n = 0; n < noutput_items; n++) {
    sum1 += in[n];
    sum2 += sum1;

    out[n] = delay[pos + 1] - sum2 * scale;

    sum2 -= delay[pos];
    delay[pos] = sum1;
    sum1 -= delay[pos + 1];
    delay[pos + 1] = in[n];

    pos += 2;
    if (pos >= 2 * length)
      pos = 0;
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
