/* -*- c++ -*- */
/*
 * Copyright 2013 Free Software Foundation, Inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FIXED_TUNTAP_PDU_IMPL_H
#define INCLUDED_MARMOTE3_FIXED_TUNTAP_PDU_IMPL_H

#include <marmote3/fixed_tuntap_pdu.h>

#ifndef __APPLE__

#include <gnuradio/thread/thread.h>
#include <pmt/pmt.h>

namespace gr {
namespace marmote3 {

class fixed_tuntap_pdu_impl : public fixed_tuntap_pdu {
private:
  const pmt::pmt_t in_port;
  const pmt::pmt_t out_port;
  const int frame_len; // mtu for IP, mtu + 14 for Ethernet

  int fd;
  gr::thread::thread thread;

private:
  static int tun_alloc(char *dev, int flags);
  static int set_mtu(const char *dev, int mtu);

public:
  fixed_tuntap_pdu_impl(const std::string &dev, int mtu, bool istunflag);
  ~fixed_tuntap_pdu_impl();

  void send(pmt::pmt_t message);
  void work();
};

} // namespace marmote3
} // namespace gr

#endif // __APPLE__

#endif /* INCLUDED_MARMOTE3_FIXED_TUNTAP_PDU_IMPL_H */
