/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_KEEP_LARGE_SEGMENTS_IMPL_H
#define INCLUDED_MARMOTE3_KEEP_LARGE_SEGMENTS_IMPL_H

#include <atomic>
#include <marmote3/keep_large_segments.h>

namespace gr {
namespace marmote3 {

class keep_large_segments_impl : public keep_large_segments {
private:
  const int itemsize;
  const long skip;
  const long keep;
  const long period;
  const long total;
  const int report_id;

  long enabled_consumed;
  std::atomic<long> total_consumed;
  std::atomic<long> total_produced;
  std::atomic<bool> atomic_enabled;

public:
  keep_large_segments_impl(int itemsize, bool enabled, long skip, long keep,
                           long period, long total, int report_id);
  ~keep_large_segments_impl();

  void forecast(int noutput_items, gr_vector_int &ninput_items_required);

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items);

  void execute(const unsigned char *input, long input_len,
               unsigned char *output, long output_len, long &consumed,
               long &produced);

  void set_enabled(bool enabled) override;
  bool get_enabled() override { return atomic_enabled; }

  void collect_block_report(ModemReport *modem_report) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_KEEP_LARGE_SEGMENTS_IMPL_H */
