/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "message_to_packet_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <sstream>

namespace gr {
namespace marmote3 {

message_to_packet::sptr
message_to_packet::make(int itemsize, int max_packet_len, int max_queue_size) {
  return gnuradio::get_initial_sptr(
      new message_to_packet_impl(itemsize, max_packet_len, max_queue_size));
}

message_to_packet_impl::message_to_packet_impl(int itemsize, int max_packet_len,
                                               int max_queue_size)
    : tagged_stream_block2("message_to_packet", gr::io_signature::make(0, 0, 0),
                           gr::io_signature::make(1, 1, itemsize),
                           max_packet_len),
      itemsize(itemsize), max_queue_size(max_queue_size),
      port_id(pmt::mp("in")), dropped(0), packet_id(0) {
  if (max_packet_len <= 0)
    throw std::out_of_range("message_to_packet: invalid max packet length");

  if (itemsize != 1 && itemsize != 4 && itemsize != 8)
    throw std::out_of_range("message_to_packet: incorrect item size");

  if (max_queue_size < 0)
    throw std::out_of_range("message_to_packet: invalid maximum queue size");

  message_port_register_in(port_id);
  set_msg_handler(port_id,
                  boost::bind(&message_to_packet_impl::receive, this, _1));
}

message_to_packet_impl::~message_to_packet_impl() {}

void message_to_packet_impl::receive(pmt::pmt_t message) {
  if (max_queue_size > 0 && queue.size() >= max_queue_size) {
    dropped += 1;
    if (dropped == 1 || dropped == 10 || dropped == 100 ||
        dropped % 1000 == 0) {
      std::stringstream msg;
      msg << "message_to_packet: dropped " << dropped << " messages at "
          << alias() << std::endl;
      std::cout << msg.str();
    }
    queue.pop_back(); // dropping oldest
  }
  queue.push_front(message); // add at the front
}

int message_to_packet_impl::work(int noutput_items, gr_vector_int &ninput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  pmt::pmt_t msg;
  if (!queue.empty()) {
    msg = queue.front();
    queue.pop_front();
  } else
    msg = delete_head_blocking(port_id);

  if (!pmt::is_pair(msg) || !pmt::is_uniform_vector(pmt::cdr(msg)))
    std::cout << "####### message_to_packet: invalid message format\n";
  else {
    pmt::pmt_t data = pmt::cdr(msg);
    pmt::pmt_t meta = pmt::car(msg);

    if (pmt::length(data) > noutput_items)
      std::cout << "####### message_to_packet: message is too long\n";
    else if (pmt::length(data) <= 0)
      std::cout << "####### message_to_packet: empty message\n";
    else if (itemsize != pmt::uniform_vector_itemsize(data))
      std::cout << "####### message_to_packet: invalid item size\n";
    else {
      size_t len(0);
      const unsigned char *in =
          (const unsigned char *)pmt::uniform_vector_elements(data, len);
      assert(len == pmt::length(data) * itemsize);

      unsigned char *out = (unsigned char *)output_items[0];
      std::memcpy(out, in, len);

      if (pmt::is_dict(meta))
        output_dict = meta;
      else
        output_dict = PMT_EMPTY_DICT;

      set_output_long(PMT_PACKET_ID, ++packet_id);
      return pmt::length(data);
    }
  }

  return 0;
}

} /* namespace marmote3 */
} /* namespace gr */
