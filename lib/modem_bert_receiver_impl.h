/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_BERT_RECEIVER_IMPL_H
#define INCLUDED_MARMOTE3_MODEM_BERT_RECEIVER_IMPL_H

#include <marmote3/modem_bert_receiver.h>
#include <random>
#include <vector>

namespace gr {
namespace marmote3 {

class modem_bert_receiver_impl : public modem_bert_receiver {
private:
  std::mt19937 engine;
  std::vector<uint8_t> packet;
  void extend_packet(int length);

  long total_frames, correct_frames;
  long total_bits, bit_errors;

public:
  modem_bert_receiver_impl(int seed);
  ~modem_bert_receiver_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

  bool stop();

  double get_ber() const;
  double get_fer() const;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_BERT_RECEIVER_IMPL_H */
