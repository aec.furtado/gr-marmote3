/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "modem_bert_sender_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <random>

namespace gr {
namespace marmote3 {

modem_bert_sender::sptr modem_bert_sender::make(int seed, int num_packets,
                                                int max_packet_len) {
  return gnuradio::get_initial_sptr(
      new modem_bert_sender_impl(seed, num_packets, max_packet_len));
}

modem_bert_sender_impl::modem_bert_sender_impl(int seed, int num_packets,
                                               int max_packet_len)
    : tagged_stream_block2("modem_bert_sender", gr::io_signature::make(0, 0, 0),
                           gr::io_signature::make(1, 1, sizeof(unsigned char)),
                           max_packet_len),
      num_packets(num_packets), max_packet_len(max_packet_len), packet_id(0) {
  std::mt19937 engine(seed);
  std::uniform_int_distribution<> rand_byte(0, 255);
  for (int i = 0; i < max_packet_len; i++)
    packet.push_back(rand_byte(engine));
}

modem_bert_sender_impl::~modem_bert_sender_impl() {}

int modem_bert_sender_impl::work(int noutput_items, gr_vector_int &ninput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  if (packet_id >= num_packets) {
    std::cerr << "BER Test sender stopping for today" << std::endl;
    return WORK_DONE;
  }
  packet_id += 1;

  uint8_t *out = (unsigned char *)output_items[0];
  std::memcpy(out, packet.data(), max_packet_len);

  output_dict = PMT_EMPTY_DICT;
  set_output_long(PMT_PACKET_ID, packet_id);

  return max_packet_len;
}

} /* namespace marmote3 */
} /* namespace gr */
