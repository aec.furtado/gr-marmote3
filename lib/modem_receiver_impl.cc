/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "modem_receiver_impl.h"
#include "modem_sender2_impl.h"
#include <arpa/inet.h>
#include <boost/crc.hpp>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/headers.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

modem_receiver::sptr modem_receiver::make(int payload_mode,
                                          modem_sender2::sptr modem_sender,
                                          bool debug_scheduler) {
  return gnuradio::get_initial_sptr(
      new modem_receiver_impl(payload_mode, modem_sender, debug_scheduler));
}

modem_receiver_impl::modem_receiver_impl(int payload_mode,
                                         modem_sender2::sptr modem_sender,
                                         bool debug_scheduler)
    : tagged_stream_block2("modem_receiver",
                           gr::io_signature::make(1, 1, sizeof(uint8_t)),
                           gr::io_signature::make(0, 0, 0), 0),
      payload_mode(payload_mode), port_id(pmt::mp("out")),
      modem_sender(modem_sender), debug_scheduler(debug_scheduler) {
  if (payload_mode < 0 || payload_mode > 4)
    throw std::out_of_range("modem_receiver: invalid payload mode");

  message_port_register_out(port_id);
}

modem_receiver_impl::~modem_receiver_impl() {}

int modem_receiver_impl::work(int noutput_items, gr_vector_int &ninput_items,
                              gr_vector_const_void_star &input_items,
                              gr_vector_void_star &output_items) {
  const uint8_t *in = (const uint8_t *)input_items[0];
  int data_len = ninput_items[0];

  if (data_len <
      sizeof(arq_header_t) + mmt_header_t::len() + sizeof(uint16_t)) {
    std::cout << "####### modem_receiver: packet is too short\n";
    return 0;
  }

  flow_key_t flow_key;
  flow_key.channel = get_input_long(0, PMT_CHANNEL_NUM, -1);
  flow_key.mcs = get_input_long(0, PMT_PACKET_MCS, -1);

  rate_key_t rate_key;
  rate_key.mcs = flow_key.mcs;
  rate_key.snr = std::round(get_input_float(0, PMT_EQU_SNR, 0.0f));

  flow_seqno_key_t flow_seqno_key;

  long bytes_received_delta = 0;

  boost::crc_16_type crc16;
  crc16.process_bytes(in, data_len - sizeof(uint16_t));
  uint16_t crc = crc16.checksum() ^ 0x713A;

  arq_header_t arq_header;
  std::memcpy(&arq_header, in, sizeof(arq_header_t));
  int extra = (unsigned int)arq_header.extra;
  int payload_len = data_len - sizeof(arq_header_t) - extra - sizeof(uint16_t);

  if (payload_len >= mmt_header_t::len() &&
      *(uint16_t *)(in + data_len - sizeof(uint16_t)) == htons(crc)) {

    pmt::pmt_t payload =
        pmt::init_u8vector(payload_len, in + sizeof(arq_header_t) + extra);
    message_port_pub(port_id, pmt::cons(output_dict, payload));

    mmt_header_t mmt;
    mmt.load(in + sizeof(arq_header_t) + extra);
    flow_key.source = mmt.src;
    flow_key.destination = mmt.dst;
    flow_key.flowid = mmt.flowid;
    flow_key.crc = true;

    flow_seqno_key.source = mmt.src;
    flow_seqno_key.destination = mmt.dst;
    flow_seqno_key.flowid = mmt.flowid;

    bytes_received_delta = mmt_header_t::get_zip_payload_len(
        in + sizeof(arq_header_t) + extra, payload_len, payload_mode);

    if (debug_scheduler) {
      modem_sender2_impl::time_point_t now = std::chrono::system_clock::now();
      int seqno = -1;
      modem_sender2_impl::time_point_t mgen =
          modem_sender2_impl::get_mgen_timestamp(
              in + sizeof(arq_header_t) + extra, payload_len, now, seqno);

      double mgen2 = modem_sender2_impl::get_time_point_double(mgen);
      double trig2 = get_input_long(0, PMT_TRIG_TIME, 0.0) * 1e-6;
      double now2 = modem_sender2_impl::get_time_point_double(now);

      std::stringstream str;
      str << alias() << ": receive flow " << mmt.flowid << " seqno " << seqno
          << std::fixed << std::setprecision(3) << " mgen " << mgen2 << " trig "
          << trig2 << " elapsed " << (trig2 - mgen2) << " now " << now2
          << " elapsed " << (now2 - mgen2) << std::endl;
      std::cout << str.str();
    }
  } else {
    flow_key.source = get_input_long(0, PMT_PACKET_SRC, -1);
    flow_key.destination = -1;
    flow_key.flowid = -1;
    flow_key.crc = false;
  }

  gr::thread::scoped_lock lock(mutex);

  flow_val_t &flow_val = flows[flow_key];

  flow_val.packets_received += 1;
  flow_val.bytes_received += bytes_received_delta;
  flow_val.power += get_input_float(0, PMT_EQU_PWR, 0.0f);
  flow_val.snr += get_input_float(0, PMT_EQU_SNR, 0.0f);
  flow_val.freq_err1 += get_input_float(0, PMT_FREQ_ERR, 0.0f);
  flow_val.phase_err += get_input_float(0, PMT_PHASE_ERR, 0.0f);
  flow_val.freq_err0 += get_input_float(0, PMT_PREAMB_FREQ, 0.0f);

  flow_seqno_val_t &flow_seqno_val = flow_seqnos[flow_seqno_key];

  if (flow_key.crc) {
    unsigned int missed = arq_header_t::get_flow_missed_count(
        arq_header.flow_seqno, flow_seqno_val.last_seqno);
    flow_val.packets_missing += missed;

    flow_seqno_val.last_seqno = arq_header.flow_seqno;
    flow_seqno_val.age = 0;

    // do ARQ proc in sender b/c packets arrive out of order and there we spin
    if (modem_sender) {
      std::vector<uint8_t> report;
      for (int i = 0; i < extra; i++)
        report.push_back(*(in + sizeof(arq_header_t) + i));

      if (!report.empty() && report.back() == 0)
        report.pop_back();

      modem_sender->process_received_arq_data(flow_seqno_key.source,
                                              flow_seqno_key.destination,
                                              arq_header.srcdst_seqno, report);
    }
  }

  rate_val_t &rate_val = rates[rate_key];
  if (flow_key.crc)
    rate_val.passed += 1;
  else
    rate_val.failed += 1;

  return 0;
}

void modem_receiver_impl::collect_block_report(ModemReport *modem_report) {
  ModemReceiverReport *report = modem_report->mutable_modem_receiver();

  gr::thread::scoped_lock lock(mutex);

  for (auto pair : flows) {
    ModemReceiverFlow *flow = report->add_flows();
    flow->set_source(pair.first.source);
    flow->set_destination(pair.first.destination);
    flow->set_flowid(pair.first.flowid);
    flow->set_channel(pair.first.channel);
    flow->set_mcs(pair.first.mcs);
    flow->set_crc(pair.first.crc);
    flow->set_packets(pair.second.packets_received);
    flow->set_packets_missing(pair.second.packets_missing);
    flow->set_power(pair.second.power);
    flow->set_snr(pair.second.snr);
    flow->set_freq_err0(pair.second.freq_err0);
    flow->set_freq_err1(pair.second.freq_err1);
    flow->set_phase_err(pair.second.phase_err);
    flow->set_bytes(pair.second.bytes_received);
  }

  for (auto pair : rates) {
    ModemReceiverRate *rate = report->add_rates();
    rate->set_mcs(pair.first.mcs);
    rate->set_snr(pair.first.snr);
    rate->set_passed(pair.second.passed);
    rate->set_failed(pair.second.failed);
  }

  flows.clear();
  rates.clear();

  {
    // TODO: magic number here
    auto iter = flow_seqnos.begin();
    while (iter != flow_seqnos.end()) {
      if (++(iter->second.age) > 120)
        iter = flow_seqnos.erase(iter);
      else
        ++iter;
    }
  }
}

} // namespace marmote3
} // namespace gr
