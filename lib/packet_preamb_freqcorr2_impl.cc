/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_preamb_freqcorr2_impl.h"
#include <cmath>
#include <gnuradio/fft/window.h>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_preamb_freqcorr2::sptr
packet_preamb_freqcorr2::make(int preamb_start, int preamb_length,
                              bool postamble, int precision, float max_freq_err,
                              int max_output_len) {
  return gnuradio::get_initial_sptr(new packet_preamb_freqcorr2_impl(
      preamb_start, preamb_length, postamble, precision, max_freq_err,
      max_output_len));
}

packet_preamb_freqcorr2_impl::packet_preamb_freqcorr2_impl(
    int preamb_start, int preamb_length, bool postamble, int precision,
    float max_freq_err, int max_output_len)
    : tagged_stream_block2("packet_preamb_freqcorr2",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      preamb_start(preamb_start), preamb_length(preamb_length),
      postamble(postamble), fft_length(preamb_length * precision),
      pos_to_rad(2.0f * M_PI / std::max(1, fft_length)),
      max_freq_pos(std::ceil(max_freq_err / pos_to_rad)),
      window(gr::fft::window::hamming(preamb_length)),
      fft(fft_length, true, 1) {
  if (preamb_start < 0)
    throw std::invalid_argument(
        "packet_preamb_freqcorr2: preamble start cannot be negative");

  if (preamb_length <= 1)
    throw std::invalid_argument(
        "packet_preamb_freqcorr2: preamble length is too small");

  if (precision <= 0)
    throw std::invalid_argument(
        "packet_preamb_freqcorr2: precision must be positive");

  if (max_freq_err <= 0.0f)
    throw std::invalid_argument(
        "packet_preamb_freqcorr2: maximum freq error must be positive");

  if (max_output_len < preamb_start + preamb_length)
    throw std::invalid_argument(
        "packet_preamb_freqcorr2: maximum output length is too small");

  std::memset(fft.get_inbuf(), 0,
              sizeof(gr_complex) * preamb_length * precision);
}

packet_preamb_freqcorr2_impl::~packet_preamb_freqcorr2_impl() {}

int packet_preamb_freqcorr2_impl::work(int noutput_items,
                                       gr_vector_int &ninput_items,
                                       gr_vector_const_void_star &input_items,
                                       gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];
  int len = ninput_items[0];

  if (preamb_start + preamb_length > len) {
    std::cout << "packet_preamb_freqcorr2: ####### input packet is too short\n";
    return 0;
  }

  const gr_complex *preamb_ptr;
  if (!postamble)
    preamb_ptr = in + preamb_start;
  else
    preamb_ptr = in + len - preamb_start - preamb_length;

  float freq_err = method1(preamb_ptr);
  set_output_float(PMT_PREAMB_FREQ, freq_err);

  gr_complex offset(std::cos(-freq_err), std::sin(-freq_err));
  gr_complex start(1.0f, 0.0f);
  volk_32fc_s32fc_x2_rotator_32fc(out, in, offset, &start, len);

  return len;
}

float packet_preamb_freqcorr2_impl::method1(const gr_complex *preamble) {
  volk_32fc_32f_multiply_32fc(fft.get_inbuf(), preamble, window.data(),
                              preamb_length);
  // make sure that the rest of the buffer is still zeroed out
  assert(fft_length <= preamb_length ||
         *(fft.get_inbuf() + fft_length - 1) == gr_complex(0.0f, 0.0f));
  fft.execute();

  int pos = 0;
  float norm = std::norm(fft.get_outbuf()[0]);
  for (int p = 1; p <= max_freq_pos; p++) {
    float n = std::norm(fft.get_outbuf()[p]);
    if (n > norm) {
      norm = n;
      pos = p;
    }
    n = std::norm(fft.get_outbuf()[fft_length - p]);
    if (n > norm) {
      norm = n;
      pos = -p;
    }
  }

  return pos * pos_to_rad;
}

} /* namespace marmote3 */
} /* namespace gr */
