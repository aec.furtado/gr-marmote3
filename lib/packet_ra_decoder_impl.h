/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_RA_DECODER_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_RA_DECODER_IMPL_H

#include <marmote3/fec_ra_xcoder.h>
#include <marmote3/packet_ra_decoder.h>
#include <unordered_map>

namespace gr {
namespace marmote3 {

class packet_ra_decoder_impl : public packet_ra_decoder {
private:
  const int data_len;
  const int code_len;
  const int full_passes;
  const int half_passes;

  std::unordered_map<uint64_t, fec_ra_decoder *> decoders;
  fec_ra_decoder *get_decoder(int data_len2, int code_len2);

public:
  packet_ra_decoder_impl(int data_len, int code_len, int full_passes,
                         int half_passes);
  packet_ra_decoder_impl(const packet_ra_decoder_impl &other) = delete;
  ~packet_ra_decoder_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_RA_DECODER_IMPL_H */
