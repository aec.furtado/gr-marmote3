/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath, Miklos Maroti
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_rep_decoder_impl.h"
#include <gnuradio/io_signature.h>
#include <random>

namespace gr {
namespace marmote3 {

packet_rep_decoder::sptr packet_rep_decoder::make(int data_len,
                                                  int repetition) {
  return gnuradio::get_initial_sptr(
      new packet_rep_decoder_impl(data_len, repetition));
}

packet_rep_decoder_impl::packet_rep_decoder_impl(int data_len, int repetition)
    : tagged_stream_block2(
          "packet_rep_decoder", gr::io_signature::make(1, 1, sizeof(char)),
          gr::io_signature::make(1, 1, sizeof(unsigned char)), data_len),
      repetition(repetition) {}

packet_rep_decoder_impl::~packet_rep_decoder_impl() {}

int packet_rep_decoder_impl::work(int noutput_items,
                                  gr_vector_int &ninput_items,
                                  gr_vector_const_void_star &input_items,
                                  gr_vector_void_star &output_items) {
  const char *in = (const char *)input_items[0];
  uint8_t *out = (uint8_t *)output_items[0];
  int len = ninput_items[0];

  if (len % (8 * repetition) != 0) {
    std::cout << "####### packet_rep_decoder: invalid packet length\n";
    return 0;
  }

  len = len / (8 * repetition);
  if (len > noutput_items) {
    std::cout << "####### packet_rep_decoder: packet too long\n";
    return 0;
  }

  std::minstd_rand engine(1); // TODO: set seed
  std::uniform_int_distribution<> rand_byte(0, 255);

  for (int i = 0; i < len; ++i) {
    int acc[8];
    for (int j = 0; j < 8; ++j)
      acc[j] = 0;

    for (int r = 0; r < repetition; r++) {
      uint8_t x = rand_byte(engine);
      for (int j = 0; j < 8; ++j) {
        if ((x & (1 << j)) == 0)
          acc[j] += *(in++);
        else
          acc[j] -= *(in++);
      }
    }

    int a = 0;
    for (int j = 0; j < 8; ++j) {
      if (acc[j] < 0)
        a |= 1 << j;
    }
    *(out++) = a;
  }

  return len;
}

} /* namespace marmote3 */
} /* namespace gr */
