/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "qa_headers.h"
#include <cassert>
#include <cppunit/TestAssert.h>
#include <cstring>
#include <ctime>
#include <gnuradio/attributes.h>
#include <iomanip>
#include <stddef.h>

namespace gr {
namespace marmote3 {

qa_headers::qa_headers()
    : engine(time(NULL)), rand_bit(0, 1), rand_byte(0, 255),
      rand_word(0, 65535) {}

void qa_headers::print_hex(const void *ptr, int len) {
  std::cout << std::right << std::setfill('0') << "[";
  for (int i = 0; i < len; i++)
    std::cout << " " << std::hex << std::setw(2)
              << (unsigned int)((const uint8_t *)ptr)[i];
  std::cout << "]" << std::endl;
}

void qa_headers::generate_eth_raw(std::vector<uint8_t> &pkt) {
  // dst
  if (rand_bit(engine)) {
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
  } else {
    pkt.push_back(0x12);
    pkt.push_back(0x34);
    pkt.push_back(0x56);
    pkt.push_back(0x78);
    pkt.push_back(0x90);
  }
  pkt.push_back(rand_byte(engine));

  // src
  if (rand_bit(engine)) {
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
    pkt.push_back(0xff);
  } else {
    pkt.push_back(0x12);
    pkt.push_back(0x34);
    pkt.push_back(0x56);
    pkt.push_back(0x78);
    pkt.push_back(0x90);
  }
  pkt.push_back(rand_byte(engine));

  // 802.1q tag
  if (rand_bit(engine)) {
    pkt.push_back(0x81);
    pkt.push_back(0x00);
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }

  // ethertype
  if (rand_bit(engine)) {
    pkt.push_back(0x08);
    pkt.push_back(0x00);
  } else {
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }
}

void qa_headers::generate_ip4_raw(std::vector<uint8_t> &pkt) {
  int start = pkt.size();

  // ver and ihl
  if (rand_bit(engine))
    pkt.push_back(0x45);
  else if (rand_bit(engine))
    pkt.push_back(0x4f);
  else
    pkt.push_back(rand_byte(engine));

  // tos
  pkt.push_back(rand_byte(engine));

  // tlen
  if (rand_bit(engine))
    pkt.push_back(0x00);
  else
    pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));

  // id
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));

  // frag
  if (rand_bit(engine)) {
    pkt.push_back(0x40);
    pkt.push_back(0x00);
  } else {
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }

  // ttl
  pkt.push_back(rand_byte(engine));

  // prot
  if (rand_bit(engine))
    pkt.push_back(0x11);
  else
    pkt.push_back(rand_byte(engine));

  // crc left empty
  pkt.push_back(0x00);
  pkt.push_back(0x00);

  // src
  if (rand_bit(engine)) {
    pkt.push_back(192);
    pkt.push_back(168);
  } else if (rand_bit(engine)) {
    pkt.push_back(0xff);
    pkt.push_back(0xff);
  } else {
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));

  // dst
  if (rand_bit(engine)) {
    pkt.push_back(192);
    pkt.push_back(168);
  } else if (rand_bit(engine)) {
    pkt.push_back(0xff);
    pkt.push_back(0xff);
  } else {
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));

  // opt
  int ip4_len = 4 * (pkt[start] & 0x0f);
  for (int i = 20; i < ip4_len; i++)
    pkt.push_back(rand_byte(engine));

  // crc calculated
  if (rand_bit(engine)) {
    uint16_t crc = ip4_crc(pkt.data() + start, ip4_len);
    pkt[start + 10] = crc >> 8;
    pkt[start + 11] = crc;
  }
}

void qa_headers::generate_eth_mem(eth_header_t &hdr) {
  // dsc
  if (rand_bit(engine)) {
    hdr.dst[0] = 0x12;
    hdr.dst[1] = 0x34;
    hdr.dst[2] = 0x56;
    hdr.dst[3] = 0x78;
    hdr.dst[4] = 0x90;
  } else if (rand_bit(engine)) {
    hdr.dst[0] = 0xff;
    hdr.dst[1] = 0xff;
    hdr.dst[2] = 0xff;
    hdr.dst[3] = 0xff;
    hdr.dst[4] = 0xff;
  } else {
    hdr.dst[0] = rand_byte(engine);
    hdr.dst[1] = rand_byte(engine);
    hdr.dst[2] = rand_byte(engine);
    hdr.dst[3] = rand_byte(engine);
    hdr.dst[4] = rand_byte(engine);
  }
  hdr.dst[5] = rand_byte(engine);

  // src
  if (rand_bit(engine)) {
    hdr.src[0] = 0x12;
    hdr.src[1] = 0x34;
    hdr.src[2] = 0x56;
    hdr.src[3] = 0x78;
    hdr.src[4] = 0x90;
  } else if (rand_bit(engine)) {
    hdr.src[0] = 0xff;
    hdr.src[1] = 0xff;
    hdr.src[2] = 0xff;
    hdr.src[3] = 0xff;
    hdr.src[4] = 0xff;
  } else {
    hdr.src[0] = rand_byte(engine);
    hdr.src[1] = rand_byte(engine);
    hdr.src[2] = rand_byte(engine);
    hdr.src[3] = rand_byte(engine);
    hdr.src[4] = rand_byte(engine);
  }
  hdr.src[5] = rand_byte(engine);

  // 802.1q tag
  if (rand_bit(engine)) {
    if (rand_bit(engine))
      hdr.tci = 0xffff;
    else if (rand_bit(engine))
      hdr.tci = 0x8100;
  } else {
    if (rand_bit(engine))
      hdr.tci = 0x0800;
    else
      hdr.tci = rand_word(engine);
  }

  // ethertype
  if (rand_bit(engine)) {
    if (rand_bit(engine))
      hdr.typ = 0xffff;
    else if (rand_bit(engine))
      hdr.typ = 0x8100;
  } else {
    if (rand_bit(engine))
      hdr.typ = 0x0800;
    else
      hdr.typ = rand_word(engine);
  }
}

void qa_headers::generate_ip4_mem(ip4_header_t &hdr) {
  hdr.ihl = rand_byte(engine) & 0x0f;
  hdr.tos = rand_byte(engine);
  hdr.tlen = rand_byte(engine) + 10; // intentionally smaller
  hdr.id = rand_word(engine);

  if (rand_bit(engine))
    hdr.frag = 0x4000;
  else
    hdr.frag = rand_word(engine);

  hdr.ttl = rand_byte(engine);

  if (rand_bit(engine))
    hdr.prot = 0x11;
  else
    hdr.prot = rand_byte(engine);

  if (rand_bit(engine)) {
    hdr.src[0] = 192;
    hdr.src[1] = 168;
  } else {
    hdr.src[0] = rand_byte(engine);
    hdr.src[1] = rand_byte(engine);
  }
  hdr.src[2] = rand_byte(engine);
  hdr.src[3] = rand_byte(engine);

  if (rand_bit(engine)) {
    hdr.dst[0] = 192;
    hdr.dst[1] = 168;
  } else {
    hdr.dst[0] = rand_byte(engine);
    hdr.dst[1] = rand_byte(engine);
  }
  hdr.dst[2] = rand_byte(engine);
  hdr.dst[3] = rand_byte(engine);

  for (int i = 0; i < 40; i++)
    hdr.opt[i] = rand_byte(engine);
}

void qa_headers::generate_udp_raw(std::vector<uint8_t> &pkt) {
  uint16_t port = rand_word(engine);
  pkt.push_back(port >> 8);
  pkt.push_back(port & 0xff);

  if (rand_bit(engine)) {
    if (rand_bit(engine))
      port += 1000;
    else
      port -= 1000;
  } else
    port = rand_word(engine);
  pkt.push_back(port >> 8);
  pkt.push_back(port & 0xff);

  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));
}

void push32(std::vector<uint8_t> &pkt, uint32_t val) {
  pkt.push_back(val >> 24);
  pkt.push_back(val >> 16);
  pkt.push_back(val >> 8);
  pkt.push_back(val);
}

void qa_headers::generate_mgn_raw(const ip4_header_t &ip4, uint16_t dst_port,
                                  std::vector<uint8_t> &pkt) {
  // messageSize
  pkt.push_back(rand_byte(engine));
  pkt.push_back(rand_byte(engine));

  // version and flags
  pkt.push_back(rand_bit(engine) ? 4 : rand_byte(engine));
  pkt.push_back(rand_bit(engine) ? 0xc : rand_byte(engine));

  // flowid, seqno
  push32(pkt, rand_bit(engine) ? dst_port : rand_word(engine));
  push32(pkt, rand_word(engine));

  // frag
  push32(pkt, rand_bit(engine) ? 0 : rand_word(engine));

  // txTime
  push32(pkt, (rand_word(engine) << 16) + rand_word(engine));
  push32(pkt, rand_bit(engine) ? rand_word(engine) : rand_word(engine) << 16);

  // dst port, type, len
  if (rand_bit(engine)) {
    pkt.push_back(dst_port >> 8);
    pkt.push_back(dst_port);
    pkt.push_back(1);
    pkt.push_back(4);
  } else {
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
    pkt.push_back(rand_byte(engine));
  }

  // dst addr
  if (rand_bit(engine)) {
    pkt.push_back(ip4.dst[0]);
    pkt.push_back(ip4.dst[1]);
    pkt.push_back(ip4.dst[2]);
    pkt.push_back(ip4.dst[3]);
    pkt.push_back(0);
    pkt.push_back(0);
    pkt.push_back(0);
    pkt.push_back(0);
  } else {
    for (int i = 0; i < 8; i++)
      pkt.push_back(rand_byte(engine));
  }

  // tos, maybe omitted
  if (rand_bit(engine))
    pkt.push_back(ip4.tos);
  else if (rand_bit(engine))
    pkt.push_back(rand_byte(engine));

  if (rand_bit(engine)) {
    push32(pkt, 0x04376820);
    push32(pkt, 0x04376820);
    push32(pkt, 0xfffffc19);
    push32(pkt, 0);
  }
}

void qa_headers::generate_payload(std::vector<uint8_t> &pkt) {
  int len = rand_byte(engine);
  for (int i = 0; i < len; i++)
    pkt.push_back(rand_byte(engine));
}

void qa_headers::test_eth_raw2mem() {
  std::vector<uint8_t> raw1;
  eth_header_t hdr;
  std::vector<uint8_t> raw2;

  for (int i = 0; i < iterations; i++) {
    raw1.clear();
    if (rand_bit(engine)) {
      generate_eth_raw(raw1);
      CPPUNIT_ASSERT(raw1.size() == 14 || raw1.size() == 18);
    }
    generate_payload(raw1);

    int len1 = hdr.eth_raw_load(raw1.data(), raw1.size());
    CPPUNIT_ASSERT(len1 == -1 || len1 == 14 || len1 == 18);
    if (len1 >= 0) {
      CPPUNIT_ASSERT(len1 == hdr.eth_raw_len());

      raw2.resize(len1);
      int len2 = hdr.eth_raw_save(raw2.data());

      CPPUNIT_ASSERT(len2 == len1);
      CPPUNIT_ASSERT(std::memcmp(raw1.data(), raw2.data(), len1) == 0);
    }
  }
}

void qa_headers::test_eth_zip2mem() {
  std::vector<uint8_t> zip1;
  eth_header_t hdr;
  std::vector<uint8_t> zip2;

  for (int i = 0; i < iterations; i++) {
    zip1.clear();
    generate_payload(zip1);

    mmt_header_t mmt1;
    mmt1.flags = rand_word(engine) & FLAGS_ETH_MSK;
    int len1 = hdr.eth_zip_load(mmt1, zip1.data(), zip1.size());
    CPPUNIT_ASSERT(len1 <= (int)zip1.size());
    if (len1 >= 0) {
      mmt_header_t mmt2;
      int len2 = hdr.eth_zip_save1(mmt2);
      CPPUNIT_ASSERT(0 <= len2 && len2 <= len1);
      CPPUNIT_ASSERT((mmt1.flags & mmt2.flags) == mmt1.flags);
    }
  }
}

void qa_headers::test_eth_mem2raw() {
  eth_header_t hdr1;
  std::vector<uint8_t> raw;
  eth_header_t hdr2;

  for (int i = 0; i < iterations; i++) {
    generate_eth_mem(hdr1);

    int len1 = hdr1.eth_raw_len();
    CPPUNIT_ASSERT(len1 == -1 || len1 == 14 || len1 == 18);

    if (len1 >= 0) {
      raw.resize(len1);
      int len2 = hdr1.eth_raw_save(raw.data());
      CPPUNIT_ASSERT(len2 == len1);

      int len3 = hdr2.eth_raw_load(raw.data(), raw.size());
      CPPUNIT_ASSERT(len3 == len2);
      CPPUNIT_ASSERT(hdr1 == hdr2);
    }
  }
}

void qa_headers::test_eth_mem2zip() {
  eth_header_t hdr1;
  std::vector<uint8_t> zip;
  eth_header_t hdr2;

  for (int i = 0; i < iterations; i++) {
    generate_eth_mem(hdr1);

    mmt_header_t mmt;
    int len1 = hdr1.eth_zip_save1(mmt);
    if (len1 >= 0) {
      CPPUNIT_ASSERT((mmt.flags & ~FLAGS_ETH_MSK) == 0);

      zip.resize(len1);
      int len2 = hdr1.eth_zip_save2(mmt, zip.data());
      CPPUNIT_ASSERT(len2 == len1);

      int len3 = hdr2.eth_zip_load(mmt, zip.data(), zip.size());
      CPPUNIT_ASSERT(len3 == len1);
      CPPUNIT_ASSERT(hdr1 == hdr2);
    }
  }
}

void qa_headers::test_ip4_raw2mem() {
  std::vector<uint8_t> raw1;
  ip4_header_t hdr;
  std::vector<uint8_t> raw2;

  for (int i = 0; i < iterations; i++) {
    raw1.clear();
    if (rand_bit(engine)) {
      generate_ip4_raw(raw1);
      CPPUNIT_ASSERT(raw1.size() >= 20);
    }
    generate_payload(raw1);

    int len1 = hdr.ip4_raw_load(raw1.data(), raw1.size());
    CPPUNIT_ASSERT(len1 == -1 || len1 >= 20);

    if (len1 >= 0) {
      CPPUNIT_ASSERT(len1 == hdr.ip4_raw_len());

      raw2.resize(len1);
      int len2 = hdr.ip4_raw_save(raw2.data());

      CPPUNIT_ASSERT(len2 == len1);
      CPPUNIT_ASSERT(std::memcmp(raw1.data(), raw2.data(), len1) == 0);
    }
  }
}

void qa_headers::test_ip4_zip2mem() {
  std::vector<uint8_t> zip1;
  ip4_header_t hdr;
  std::vector<uint8_t> zip2;

  for (int i = 0; i < iterations; i++) {
    zip1.clear();
    generate_payload(zip1);

    mmt_header_t mmt1;
    mmt1.flags = rand_word(engine) & FLAGS_IP4_MSK;
    int len1 = hdr.ip4_zip_load1(mmt1, zip1.data(), zip1.size());
    CPPUNIT_ASSERT(len1 <= (int)zip1.size());
    if (len1 >= 0) {
      int payload_len = zip1.size() - len1;
      hdr.ip4_zip_load2(mmt1, payload_len);

      zip2.resize(zip1.size());
      mmt_header_t mmt2;
      int len2 = hdr.ip4_zip_save1(mmt2, payload_len);

      CPPUNIT_ASSERT(0 <= len2 && len2 <= len1);
    }
  }
}

void qa_headers::test_ip4_mem2raw() {
  ip4_header_t hdr1;
  std::vector<uint8_t> raw;
  ip4_header_t hdr2;

  for (int i = 0; i < iterations; i++) {
    generate_ip4_mem(hdr1);

    int len1 = hdr1.ip4_raw_len();
    CPPUNIT_ASSERT(len1 == -1 || (20 <= len1 && len1 <= 60));

    if (len1 >= 0) {
      raw.resize(len1);
      int len2 = hdr1.ip4_raw_save(raw.data());
      CPPUNIT_ASSERT(len2 == len1);

      int len3 = hdr2.ip4_raw_load(raw.data(), raw.size());
      CPPUNIT_ASSERT(len3 == len2);
      CPPUNIT_ASSERT(hdr1 == hdr2);
    }
  }
}

void qa_headers::test_ip4_mem2zip() {
  ip4_header_t hdr1;
  std::vector<uint8_t> zip;
  ip4_header_t hdr2;

  for (int i = 0; i < iterations; i++) {
    generate_ip4_mem(hdr1);

    mmt_header_t mmt;
    mmt.src = rand_byte(engine);
    mmt.dst = rand_byte(engine);
    int payload_len = rand_byte(engine);

    int len1 = hdr1.ip4_zip_save1(mmt, payload_len);
    if (len1 >= 0) {
      CPPUNIT_ASSERT((mmt.flags & ~FLAGS_IP4_MSK) == 0);

      zip.resize(len1);
      int len2 = hdr1.ip4_zip_save2(mmt, zip.data());
      CPPUNIT_ASSERT(len2 == len1);

      int len3 = hdr2.ip4_zip_load1(mmt, zip.data(), zip.size());
      CPPUNIT_ASSERT(len3 == len1);
      hdr2.ip4_zip_load2(mmt, payload_len);

      CPPUNIT_ASSERT(hdr1 == hdr2);
    }
  }
}

void qa_headers::test_udp_raw2raw() {
  std::vector<uint8_t> raw1;
  std::vector<uint8_t> zip;
  std::vector<uint8_t> raw2;

  for (int i = 0; i < iterations; i++) {
    ip4_header_t ip4;
    generate_ip4_mem(ip4);
    ip4.prot = 0x11;
    int tmp = ip4.ip4_raw_len();
    if (tmp < 0)
      continue;

    raw1.clear();
    generate_udp_raw(raw1);
    int udp_raw_len = raw1.size();
    generate_payload(raw1);
    int ip4_payload_len = raw1.size();
    int udp_payload_len = raw1.size() - udp_raw_len;

    udp_header_t udp1;
    tmp = udp1.udp_raw_load(raw1.data(), raw1.size());
    CPPUNIT_ASSERT(tmp == udp_raw_len);

    mmt_header_t mmt;
    int ip4_zip_len = ip4.ip4_zip_save1(mmt, ip4_payload_len);
    CPPUNIT_ASSERT(ip4_zip_len >= 0);

    udp1.udp_zip_save1(mmt, ip4, raw1.data() + udp_raw_len, udp_payload_len);
    int udp_zip_len = udp_header_t::udp_zip_len(mmt);
    CPPUNIT_ASSERT(udp_zip_len >= 0);

    zip.resize(udp_zip_len + udp_payload_len);
    udp1.udp_zip_save2(mmt, zip.data());

    std::memcpy(zip.data() + udp_zip_len, raw1.data() + udp_raw_len,
                udp_payload_len);

    udp_header_t udp2;
    tmp = udp_header_t::udp_zip_len(mmt);
    CPPUNIT_ASSERT(tmp == udp_zip_len);
    udp2.udp_zip_load1(mmt, zip.data());

    udp2.udp_zip_load2(mmt, ip4, zip.data() + udp_zip_len, udp_payload_len);
    CPPUNIT_ASSERT(udp1 == udp2);

    raw2.resize(udp_raw_len + udp_payload_len);
    tmp = udp_header_t::udp_raw_len();
    CPPUNIT_ASSERT(tmp == udp_raw_len);
    udp2.udp_raw_save(raw2.data());

    std::memcpy(raw2.data() + udp_raw_len, zip.data() + udp_zip_len,
                udp_payload_len);
    CPPUNIT_ASSERT(std::memcmp(raw1.data(), raw2.data(), raw1.size()) == 0);
  }
}

void qa_headers::test_tcp_raw2raw() {
  std::vector<uint8_t> raw1;
  std::vector<uint8_t> zip;
  std::vector<uint8_t> raw2;

  for (int i = 0; i < iterations; i++) {
    ip4_header_t ip4;
    generate_ip4_mem(ip4);
    ip4.prot = 0x06;
    int tmp = ip4.ip4_raw_len();
    if (tmp < 0)
      continue;

    raw1.clear();
    generate_payload(raw1);

    tcp_header_t tcp1;
    int tcp_raw_len = tcp1.tcp_raw_load(raw1.data(), raw1.size());
    if (tcp_raw_len < 0)
      continue;
    CPPUNIT_ASSERT(tcp_raw_len <= raw1.size());
    int ip4_payload_len = raw1.size();
    int tcp_payload_len = raw1.size() - tcp_raw_len;

    mmt_header_t mmt;
    int ip4_zip_len = ip4.ip4_zip_save1(mmt, ip4_payload_len);
    CPPUNIT_ASSERT(ip4_zip_len >= 0);

    int tcp_zip_len = tcp1.tcp_zip_save1(mmt, ip4, raw1.data() + tcp_raw_len,
                                         tcp_payload_len);
    CPPUNIT_ASSERT(tcp_zip_len >= 0);

    zip.resize(tcp_zip_len + tcp_payload_len);
    tmp = tcp1.tcp_zip_save2(mmt, zip.data());
    CPPUNIT_ASSERT(tmp == tcp_zip_len);

    std::memcpy(zip.data() + tcp_zip_len, raw1.data() + tcp_raw_len,
                tcp_payload_len);

    tcp_header_t tcp2;
    tmp = tcp2.tcp_zip_load1(mmt, zip.data(), zip.size());
    CPPUNIT_ASSERT(tmp == tcp_zip_len);

    tcp2.tcp_zip_load2(mmt, ip4, zip.data() + tcp_zip_len, tcp_payload_len);
    CPPUNIT_ASSERT(tcp1 == tcp2);

    raw2.resize(tcp_raw_len + tcp_payload_len);
    tmp = tcp2.tcp_raw_save(raw2.data());
    CPPUNIT_ASSERT(tmp == tcp_raw_len);

    std::memcpy(raw2.data() + tcp_raw_len, zip.data() + tcp_zip_len,
                tcp_payload_len);
    CPPUNIT_ASSERT(std::memcmp(raw1.data(), raw2.data(), raw1.size()) == 0);
  }
}

void qa_headers::test_mgn_raw2raw() {
  std::vector<uint8_t> raw1;
  std::vector<uint8_t> zip;
  std::vector<uint8_t> raw2;

  for (int i = 0; i < iterations; i++) {
    ip4_header_t ip4;
    generate_ip4_mem(ip4);
    int tmp = ip4.ip4_raw_len();
    if (tmp < 0)
      continue;

    raw1.clear();
    uint16_t dst_port = rand_word(engine);
    if (rand_bit(engine))
      generate_mgn_raw(ip4, dst_port, raw1);
    generate_payload(raw1);

    mgn_header_t mgn1;
    int mgn_raw_len =
        mgn1.mgn_raw_load(ip4, dst_port, raw1.data(), raw1.size());
    CPPUNIT_ASSERT(mgn_raw_len >= 0 && mgn_raw_len <= raw1.size());
    int ip4_payload_len = raw1.size();
    int mgn_payload_len = raw1.size() - mgn_raw_len;

    mmt_header_t mmt;
    int ip4_zip_len = ip4.ip4_zip_save1(mmt, ip4_payload_len);
    CPPUNIT_ASSERT(ip4_zip_len >= 0);

    int mgn_zip_len = mgn1.mgn_zip_save1(mmt, ip4);
    CPPUNIT_ASSERT(mgn_zip_len >= 0);

    zip.resize(mgn_zip_len + mgn_payload_len);
    tmp = mgn1.mgn_zip_save2(mmt, zip.data());
    CPPUNIT_ASSERT(tmp == mgn_zip_len);

    std::memcpy(zip.data() + mgn_zip_len, raw1.data() + mgn_raw_len,
                mgn_payload_len);

    mgn_header_t mgn2;
    tmp = mgn2.mgn_zip_load(mmt, ip4, zip.data(), zip.size());
    CPPUNIT_ASSERT(tmp == mgn_zip_len);
    CPPUNIT_ASSERT(mgn1 == mgn2);

    raw2.resize(mgn_raw_len + mgn_payload_len);
    tmp = mgn2.mgn_raw_save(ip4, dst_port, raw2.data());
    CPPUNIT_ASSERT(tmp == mgn_raw_len);

    std::memcpy(raw2.data() + mgn_raw_len, zip.data() + mgn_zip_len,
                mgn_payload_len);
    CPPUNIT_ASSERT(std::memcmp(raw1.data(), raw2.data(), raw1.size()) == 0);
  }
}

} /* namespace marmote3 */
} /* namespace gr */
