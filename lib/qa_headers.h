/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _QA_HEADERS_H_
#define _QA_HEADERS_H_

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <marmote3/headers.h>
#include <random>
#include <vector>

namespace gr {
namespace marmote3 {

class qa_headers : public CppUnit::TestCase {
public:
  qa_headers();

  CPPUNIT_TEST_SUITE(qa_headers);
  CPPUNIT_TEST(test_eth_raw2mem);
  CPPUNIT_TEST(test_eth_zip2mem);
  CPPUNIT_TEST(test_eth_mem2raw);
  CPPUNIT_TEST(test_eth_mem2zip);
  CPPUNIT_TEST(test_ip4_raw2mem);
  CPPUNIT_TEST(test_ip4_zip2mem);
  CPPUNIT_TEST(test_ip4_mem2raw);
  CPPUNIT_TEST(test_ip4_mem2zip);
  CPPUNIT_TEST(test_udp_raw2raw);
  CPPUNIT_TEST(test_tcp_raw2raw);
  CPPUNIT_TEST(test_mgn_raw2raw);
  CPPUNIT_TEST_SUITE_END();

private:
  const int iterations = 50000;

  std::minstd_rand engine;
  std::uniform_int_distribution<> rand_bit;
  std::uniform_int_distribution<> rand_byte;
  std::uniform_int_distribution<> rand_word;

  void print_hex(const void *ptr, int len);

  void generate_eth_raw(std::vector<uint8_t> &pkt);
  void generate_ip4_raw(std::vector<uint8_t> &pkt);
  void generate_eth_mem(eth_header_t &hdr);
  void generate_ip4_mem(ip4_header_t &hdr);
  void generate_udp_raw(std::vector<uint8_t> &pkt);
  void generate_mgn_raw(const ip4_header_t &ip4, uint16_t dst_port,
                        std::vector<uint8_t> &pkt);
  void generate_payload(std::vector<uint8_t> &pkt);

  void test_eth_raw2mem();
  void test_eth_zip2mem();
  void test_eth_mem2raw();
  void test_eth_mem2zip();

  void test_ip4_raw2mem();
  void test_ip4_zip2mem();
  void test_ip4_mem2raw();
  void test_ip4_mem2zip();

  void test_udp_raw2raw();
  void test_tcp_raw2raw();
  void test_mgn_raw2raw();
};

} /* namespace marmote3 */
} /* namespace gr */

#endif /* _QA_HEADERS_H_ */
