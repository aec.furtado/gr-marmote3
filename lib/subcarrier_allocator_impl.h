/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_IMPL_H
#define INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_IMPL_H

#include <deque>
#include <limits>
#include <marmote3/subcarrier_allocator.h>
#include <unordered_map>

namespace gr {
namespace marmote3 {

class subcarrier_allocator_impl : public subcarrier_allocator {
private:
  const int vlen;
  const int max_xmitting;
  vector_peak_probe::sptr cca_peak_probe;
  const std::vector<int> subcarrier_map;
  const modem_sender2::sptr modem_sender;
  const bool debug_scheduler;

  int next_channel;

  typedef struct channel_t {
    int channel;
    bool active = false; // primary channel
    bool backup = false; // if parimary is occupied
    bool xmitting = false;
    int cca_bin = -1; // from vector peak probe
    std::vector<gr_complex> samples;
    unsigned long position = 0;
    channel_t(int channel) : channel(channel) {}
  } channel_t;
  std::vector<channel_t> channels;

  // for loading samples
  std::vector<tag_t> tags;
  const gr_complex *input_data;
  int input_length;
  int input_consumed;

  // for inhibit
  typedef struct inhibit_t {
    double start_sec;        // in seconds
    double stop_sec;         // in seconds
    unsigned long start_pos; // in nitems
    unsigned long stop_pos;  // in nitems

    inhibit_t(double start_sec, double stop_sec, unsigned long start_pos,
              unsigned long stop_pos)
        : start_sec(start_sec), stop_sec(stop_sec), start_pos(start_pos),
          stop_pos(stop_pos) {}
  } inhibit_t;

  double last_report_sec = 0.0;
  unsigned long last_report_pos = 0UL;
  double pos_per_sec = 0.0;
  std::deque<inhibit_t> inhibits;

  inline unsigned long inhibit_sec_to_pos(double sec) {
    return std::max(0L, long(last_report_pos) +
                            long((sec - last_report_sec) * pos_per_sec));
  }

  // for statistics
  struct flow_key_t {
    int channel = -1;
    int source = -1;
    int destination = -1;
    int mcs = -1;

    inline bool operator==(const flow_key_t &key) const {
      return channel == key.channel && source == key.source &&
             destination == key.destination && mcs == key.mcs;
    }
  };

  struct flow_hash_t {
    inline size_t operator()(const flow_key_t &key) const {
      return (key.source << 24) + (key.destination << 16) + (key.channel << 8) +
             key.mcs;
    }
  };

  struct flow_val_t {
    unsigned long frames = 0; // number of sent frames
    int length_min = std::numeric_limits<int>::max();
    int length_max = std::numeric_limits<int>::min();
    int cca_bin_min = std::numeric_limits<int>::max();
    int cca_bin_max = std::numeric_limits<int>::min();
  };

  std::unordered_map<flow_key_t, flow_val_t, flow_hash_t> flows;
  gr::thread::mutex mutex;

  bool load_samples_into(channel_t &chan);

public:
  subcarrier_allocator_impl(int vlen, const std::vector<int> &active_channels,
                            const std::vector<int> &backup_channels,
                            int max_xmitting,
                            vector_peak_probe::sptr cca_peak_probe,
                            const std::vector<int> &subcarrier_map,
                            modem_sender2::sptr modem_sender,
                            bool debug_scheduler);
  ~subcarrier_allocator_impl();

  void forecast(int noutput_items,
                gr_vector_int &ninput_items_required) override;

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items) override;

  static long get_input_long(const pmt::pmt_t &dict, const pmt::pmt_t &key,
                             long defval);

  void collect_block_report(ModemReport *modem_report) override;
  void process_block_command(const ModemCommand *modem_command) override;

  void set_active_channels(const std::vector<int> &active_channels) override;
  void set_backup_channels(const std::vector<int> &backup_channels) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SUBCARRIER_ALLOCATOR_IMPL_H */
