/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "subcarrier_serializer2_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>

namespace gr {
namespace marmote3 {

subcarrier_serializer2::sptr subcarrier_serializer2::make(
    int vlen, int trig_rate, float trig_level, int preamb_offset,
    int postamb_offset, bool postamble, int min_frame_len, int max_frame_len,
    const std::vector<int> &enabled) {
  return gnuradio::get_initial_sptr(new subcarrier_serializer2_impl(
      vlen, trig_rate, trig_level, preamb_offset, postamb_offset, postamble,
      min_frame_len, max_frame_len, enabled));
}

subcarrier_serializer2_impl::subcarrier_serializer2_impl(
    int vlen, int trig_rate, float trig_level, int preamb_offset,
    int postamb_offset, bool postamble, int min_frame_len, int max_frame_len,
    const std::vector<int> &enabled)
    : gr::block("subcarrier_serializer2",
                gr::io_signature::make2(2, 2, sizeof(gr_complex) * vlen,
                                        sizeof(float) * vlen),
                gr::io_signature::make(1, 1, sizeof(gr_complex))),
      block_name(pmt::string_to_symbol("subcarrier_serializer2")), vlen(vlen),
      history_len(2 * max_frame_len), trig_rate(trig_rate),
      trig_level(trig_level), preamb_offset(preamb_offset),
      postamb_offset(postamb_offset), postamble(postamble),
      min_frame_len(min_frame_len), max_frame_len(max_frame_len),
      subcarriers(vlen) {
  if (vlen <= 0)
    throw std::invalid_argument(
        "subcarrier_serializer2: vector length must be positive");

  if (trig_rate <= 0)
    throw std::invalid_argument(
        "subcarrier_serializer2: trigger rate must be positive");

  if (trig_level <= 0.0f)
    throw std::invalid_argument(
        "subcarrier_serializer2: trigger level must be positive");

  if (preamb_offset < 0 || preamb_offset >= max_frame_len)
    throw std::invalid_argument(
        "subcarrier_serializer2: invalid preamble offset");

  if (postamb_offset < 0 || postamb_offset >= max_frame_len)
    throw std::invalid_argument(
        "subcarrier_serializer2: invalid postamble offset");

  if (min_frame_len <= 0)
    throw std::invalid_argument(
        "subcarrier_serializer2: invalid min frame length");

  if (max_frame_len < min_frame_len)
    throw std::invalid_argument(
        "subcarrier_serializer2: invalid max frame length");

  for (int i = 0; i < vlen; i++) {
    subcarriers[i].enabled = false;
    subcarriers[i].active = false;
  }
  set_enabled(enabled);

  set_relative_rate(1.0);
  set_output_multiple(max_frame_len);
  set_history(1 + history_len);
  set_min_output_buffer(0, 2 * vlen * max_frame_len);
  set_tag_propagation_policy(TPP_DONT);
}

subcarrier_serializer2_impl::~subcarrier_serializer2_impl() {}

void subcarrier_serializer2_impl::forecast(
    int noutput_items, gr_vector_int &ninput_items_required) {
  // we need history and at least one sample
  ninput_items_required[0] = history_len + trig_rate;
  ninput_items_required[1] = history_len + 1;
}

int subcarrier_serializer2_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  if (false) {
    std::cout << "work: " << ninput_items[0] << " " << ninput_items[1] << " "
              << noutput_items << std::endl;
  }

  assert(noutput_items >= max_frame_len);
  gr_complex *output_ptr = (gr_complex *)output_items[0];
  int output_len = noutput_items;

  const gr_complex *input_ptr = (const gr_complex *)input_items[0];
  const float *trig_ptr = (const float *)input_items[1];

  // skipping history
  input_ptr += history_len * vlen;
  trig_ptr += history_len * vlen;
  assert(ninput_items[0] >= history_len + trig_rate &&
         ninput_items[1] >= history_len + 1);
  int input_len = std::min((ninput_items[0] - history_len) / trig_rate,
                           ninput_items[1] - history_len);

  if (false) {
    std::cout << "input_len " << input_len << " output_len " << output_len
              << std::endl;
  }

  int produced = 0;
  int consumed;
  if (!postamble)
    consumed = preamble_work(trig_ptr, input_ptr, input_len, output_ptr,
                             output_len, produced);
  else
    consumed = postamble_work(trig_ptr, input_ptr, input_len, output_ptr,
                              output_len, produced);

  if (false) {
    std::cout << "consumed " << consumed << " produced " << produced
              << std::endl;
  }

  consume(0, consumed * trig_rate);
  consume(1, consumed);
  return produced;
}

int subcarrier_serializer2_impl::preamble_work(const float *trig_ptr,
                                               const gr_complex *input_ptr,
                                               int input_len,
                                               gr_complex *output_ptr,
                                               int output_len, int &produced) {
  int consumed = 0;

  while (consumed < input_len) {
    for (int chan = 0; chan < vlen; chan++) {
      float f = *(trig_ptr++);
      if (f >= trig_level) {
        subcarrier_t &sub = subcarriers[chan];
        long pos = nitems_read(0) + consumed * trig_rate;

        if (sub.active) {
          assert(sub.trig_pos <= pos);
          long frame_start_pos = sub.trig_pos - preamb_offset;
          int frame_len = pos - frame_start_pos;
          if (frame_len >= min_frame_len) {
            frame_len = std::min(frame_len, max_frame_len);
            write_frame(frame_start_pos, frame_len, chan, input_ptr, input_len,
                        output_ptr, output_len, produced);
          }
          sub.active = false;
        }

        if (sub.enabled) {
          sub.active = true;
          sub.trig = f;
          sub.trig_pos = pos;
          sub.trig_time = std::chrono::system_clock::now();
        }
      }
    }
    consumed += 1;
  }

  for (int chan = 0; chan < vlen; chan++) {
    subcarrier_t &sub = subcarriers[chan];
    if (sub.active) {
      long pos = nitems_read(0) + consumed * trig_rate;
      assert(sub.trig_pos <= pos);
      long frame_start_pos = sub.trig_pos - preamb_offset;
      int frame_len = pos - frame_start_pos;
      if (frame_len >= max_frame_len) {
        write_frame(frame_start_pos, max_frame_len, chan, input_ptr, input_len,
                    output_ptr, output_len, produced);
        sub.active = false;
      }
    }
  }

  return consumed;
}

int subcarrier_serializer2_impl::postamble_work(const float *trig_ptr,
                                                const gr_complex *input_ptr,
                                                int input_len,
                                                gr_complex *output_ptr,
                                                int output_len, int &produced) {
  int offset1 = (postamb_offset + trig_rate - 1) / trig_rate;
  int offset2 = offset1 * trig_rate;

  int consumed = 0;

  trig_ptr -= offset1 * vlen; // just an optimization
  while (consumed < input_len) {
    for (int chan = 0; chan < vlen; chan++) {
      float f = *(trig_ptr++); // just an optimization
      if (f >= trig_level) {
        subcarrier_t &sub = subcarriers[chan];
        long pos = nitems_read(0) + consumed * trig_rate;

        if (sub.enabled) {
          assert(sub.trig_pos <= pos);
          long frame_end_pos = pos - offset2 + postamb_offset;
          long frame_start_pos = sub.trig_pos - offset2;
          int frame_len = frame_end_pos - frame_start_pos;

          sub.trig = f;
          sub.trig_pos = pos;
          sub.trig_time = std::chrono::system_clock::now();

          if (frame_len >= min_frame_len) {
            frame_len += 600; // TODO: fix this hack to get more history
            frame_len = std::min(frame_len, max_frame_len);
            frame_start_pos = frame_end_pos - frame_len;

            write_frame(frame_start_pos, frame_len, chan, input_ptr, input_len,
                        output_ptr, output_len, produced);
          }
        }
      }
    }
    consumed += 1;
  }

  return consumed;
}

void subcarrier_serializer2_impl::write_frame(
    long frame_start_pos, int frame_len, int chan, const gr_complex *input_ptr,
    int input_len, gr_complex *output_ptr, int output_len, int &produced) {
  assert(0 <= chan && chan < subcarriers.size());
  subcarrier_t &sub = subcarriers[chan];
  assert(min_frame_len <= frame_len && frame_len <= max_frame_len);

  int offset = frame_start_pos - nitems_read(0);
  assert(offset + frame_len <= input_len * trig_rate);

  {
    gr::thread::scoped_lock lock(mutex);
    flow_val_t &flow_val = flows[chan];
    flow_val.frames += 1;
    flow_val.trigger_sum += sub.trig;
    flow_val.length_min = std::min(flow_val.length_min, frame_len);
    flow_val.length_max = std::max(flow_val.length_max, frame_len);
    if (produced + frame_len > output_len || offset < -history_len) {
      flow_val.dropped += 1;
      return;
    }
  }

  if (false) {
    std::cout << "written " << chan << " " << frame_start_pos << " "
              << frame_len << std::endl;
  }

  const gr_complex *in = input_ptr + offset * vlen + chan;
  gr_complex *out = output_ptr + produced;

  for (int i = 0; i < frame_len; i++) {
    *(out++) = *in;
    in += vlen;
  }

  add_item_tag(0, nitems_written(0) + produced, PMT_PACKET_LEN,
               pmt::from_long(frame_len), block_name);

  pmt::pmt_t dict = PMT_EMPTY_DICT;
  dict = pmt::dict_add(dict, PMT_CHANNEL_NUM, pmt::from_long(chan));
  dict = pmt::dict_add(dict, PMT_TRIG_SAMP, pmt::from_long(sub.trig_pos));

  // we store the elapsed microseconds since the start of the epoch in 1970
  long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(
                       sub.trig_time.time_since_epoch())
                       .count();
  dict = pmt::dict_add(dict, PMT_TRIG_TIME, pmt::from_long(microsecs));

  add_item_tag(0, nitems_written(0) + produced, PMT_PACKET_DICT, dict,
               block_name);

  produced += frame_len;
}

void subcarrier_serializer2_impl::set_enabled(const std::vector<int> &enabled) {
  for (int i = 0; i < vlen; i++)
    subcarriers[i].enabled = false;

  for (int i = 0; i < enabled.size(); i++)
    if (0 <= enabled[i] && enabled[i] < vlen)
      subcarriers[enabled[i]].enabled = true;
    else
      std::cout << "####### subcarrier_serializer2: invalid enabled channel\n";
}

std::vector<int> subcarrier_serializer2_impl::get_enabled() {
  std::vector<int> enabled;

  for (int i = 0; i < vlen; i++)
    if (subcarriers[i].enabled)
      enabled.push_back(i);

  return enabled;
}

void subcarrier_serializer2_impl::collect_block_report(
    ModemReport *modem_report) {
  SubcarrierSerializerReport *report =
      modem_report->mutable_subcarrier_serializer();

  gr::thread::scoped_lock lock(mutex);
  for (auto pair : flows) {
    SubcarrierSerializerFlow *flow = report->add_flows();
    flow->set_channel(pair.first);
    flow->set_frames(pair.second.frames);
    flow->set_dropped(pair.second.dropped);
    if (pair.second.frames != 0)
      flow->set_trigger_avg(pair.second.trigger_sum / pair.second.frames);
    flow->set_length_min(pair.second.length_min);
    flow->set_length_max(pair.second.length_max);
  }

  flows.clear();
}

} /* namespace marmote3 */
} /* namespace gr */
