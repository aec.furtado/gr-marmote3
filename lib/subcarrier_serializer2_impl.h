/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_IMPL_H
#define INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_IMPL_H

#include <chrono>
#include <marmote3/subcarrier_serializer2.h>
#include <unordered_map>
#include <vector>

namespace gr {
namespace marmote3 {

class subcarrier_serializer2_impl : public subcarrier_serializer2 {
private:
  const pmt::pmt_t block_name;
  const int vlen;
  const int history_len;
  const int trig_rate;
  const float trig_level;
  const int preamb_offset;
  const int postamb_offset;
  const bool postamble;
  const int min_frame_len;
  const int max_frame_len;

  struct subcarrier_t {
    bool enabled; // we can trigger (receive) on this channel
    bool active;  // we are saving data (receive) on this channel
    float trig;   // trigger value for active channel
    std::chrono::system_clock::time_point trig_time;
    unsigned long trig_pos;
  };
  std::vector<subcarrier_t> subcarriers;

public:
  subcarrier_serializer2_impl(int vlen, int trig_rate, float trig_level,
                              int preamb_offset, int postamb_offset,
                              bool postamble, int min_frame_len,
                              int max_frame_len,
                              const std::vector<int> &enabled);
  ~subcarrier_serializer2_impl();

  void forecast(int noutput_items,
                gr_vector_int &ninput_items_required) override;

  int general_work(int noutput_items, gr_vector_int &ninput_items,
                   gr_vector_const_void_star &input_items,
                   gr_vector_void_star &output_items) override;

  void set_enabled(const std::vector<int> &enabled) override;
  std::vector<int> get_enabled() override;

  void collect_block_report(ModemReport *modem_report) override;

private:
  int preamble_work(const float *trig_ptr, const gr_complex *input_ptr,
                    int input_len, gr_complex *output_ptr, int output_len,
                    int &produced);

  int postamble_work(const float *trig_ptr, const gr_complex *input_ptr,
                     int input_len, gr_complex *output_ptr, int output_len,
                     int &produced);

  void write_frame(long frame_start_pos, int frame_len, int chan,
                   const gr_complex *input_ptr, int input_len,
                   gr_complex *output_ptr, int output_len, int &produced);

  struct flow_val_t {
    unsigned long frames = 0;    // number of triggered frames
    unsigned long dropped = 0;   // number of dropped frames
    float trigger_sum = 0.0f;    // total trigger value (divide by frames)
    int length_min = 0x7fffffff; // minimum frame length in samples
    int length_max = 0;          // maximum frame length in samples
  };

  std::unordered_map<int, flow_val_t> flows;
  gr::thread::mutex mutex;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_IMPL_H */
