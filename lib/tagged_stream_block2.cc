/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/block_detail.h>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/tagged_stream_block2.h>
#include <sstream>

namespace gr {
namespace marmote3 {

tagged_stream_block2::tagged_stream_block2(const std::string &name,
                                           io_signature::sptr input_signature,
                                           io_signature::sptr output_signature,
                                           int max_output_len)
    : gr::block(name, input_signature, output_signature),
      block_name_sym(pmt::string_to_symbol(name)),
      max_output_len(max_output_len),
      input_lengths(input_signature->min_streams(), 1),
      input_dicts(input_signature->min_streams(), PMT_EMPTY_DICT),
      output_dict(PMT_EMPTY_DICT) {
  if (input_signature->min_streams() != input_signature->max_streams())
    throw std::invalid_argument(
        "tagged_stream_block2: invalid input signature");

  if (output_signature->min_streams() != output_signature->max_streams())
    throw std::invalid_argument(
        "tagged_stream_block2: invalid output signature");

  if (max_output_len < 0)
    throw std::invalid_argument(
        "tagged_stream_block2: invalid max output length");

  set_tag_propagation_policy(TPP_DONT);
  set_min_noutput_items(max_output_len);

  for (int i = 0; i < output_signature->min_streams(); i++) {
    // Why is flat_flowgraph::allocate_buffer so limited?
    int item_size = output_signature->sizeof_stream_item(i);
    int buff_size = (2 * max_output_len * item_size + 4095) / 4096 * 4096;
    if (buff_size > 65536L)
      set_min_output_buffer(i, 2 * max_output_len);
    else {
      set_max_output_buffer(i, buff_size / item_size);
    }
  }
}

bool tagged_stream_block2::has_input_long(int input, const pmt::pmt_t &key) {
  assert(0 <= input && input < input_dicts.size());
  pmt::pmt_t v = pmt::dict_ref(input_dicts[input], key, pmt::get_PMT_EOF());
  return pmt::is_integer(v);
}

long tagged_stream_block2::get_input_long(int input, const pmt::pmt_t &key,
                                          long val) {
  assert(0 <= input && input < input_dicts.size());
  pmt::pmt_t v = pmt::dict_ref(input_dicts[input], key, pmt::get_PMT_EOF());
  return pmt::is_integer(v) ? pmt::to_long(v) : val;
}

bool tagged_stream_block2::has_input_float(int input, const pmt::pmt_t &key) {
  assert(0 <= input && input < input_dicts.size());
  pmt::pmt_t v = pmt::dict_ref(input_dicts[input], key, pmt::get_PMT_EOF());
  return pmt::is_real(v);
}

float tagged_stream_block2::get_input_float(int input, const pmt::pmt_t &key,
                                            float val) {
  assert(0 <= input && input < input_dicts.size());
  pmt::pmt_t v = pmt::dict_ref(input_dicts[input], key, pmt::get_PMT_EOF());
  return pmt::is_real(v) ? pmt::to_float(v) : val;
}

void tagged_stream_block2::set_output_long(const pmt::pmt_t &key, long val) {
  output_dict = pmt::dict_add(output_dict, key, pmt::from_long(val));
}

void tagged_stream_block2::set_output_float(const pmt::pmt_t &key, float val) {
  output_dict = pmt::dict_add(output_dict, key, pmt::from_float(val));
}

void tagged_stream_block2::set_output_complex_vector(
    const pmt::pmt_t &key, const std::vector<gr_complex> &val) {
  output_dict =
      pmt::dict_add(output_dict, key, pmt::init_c32vector(val.size(), val));
}

void tagged_stream_block2::forecast(int noutput_items,
                                    gr_vector_int &ninput_items_required) {
  assert(ninput_items_required.size() == input_lengths.size());
  for (int i = 0; i < ninput_items_required.size(); i++)
    ninput_items_required[i] = input_lengths[i];
}

int tagged_stream_block2::general_work(int noutput_items,
                                       gr_vector_int &ninput_items,
                                       gr_vector_const_void_star &input_items,
                                       gr_vector_void_star &output_items) {
  assert(ninput_items.size() == input_dicts.size());
  assert(noutput_items >= max_output_len);

  // get the length and dictionaries
  bool giveup = false;
  std::vector<tag_t> tags;
  for (int i = 0; i < input_dicts.size(); i++) {
    uint64_t r = nitems_read(i);
    int c = ninput_items[i];

    get_tags_in_range(tags, i, r, r + 1, PMT_PACKET_LEN);
    if (tags.size() == 1 && pmt::is_integer(tags[0].value)) {
      input_lengths[i] = std::max(1, (int)pmt::to_long(tags[0].value));
      if (c < input_lengths[i])
        giveup = true;

      get_tags_in_range(tags, i, r, r + 1, PMT_PACKET_DICT);
      if (tags.size() == 1 && pmt::is_dict(tags[0].value)) {
        input_dicts[i] = tags[0].value;
      } else {
        input_dicts[i] = PMT_EMPTY_DICT;

        // drop the dictionary on error
        if (tags.size() != 0)
          std::cout
              << "####### tagged_stream_block2: invalid packet dictionary at "
              << alias() << std::endl;
      }
    } else {
      // skip input items on missing packet length
      std::cout << "####### tagged_stream_block2: missing packet length at "
                << alias() << std::endl;

      get_tags_in_range(tags, i, r + 1, r + c);
      if (tags.size() > 0)
        c = std::max(1, (int)(tags[0].offset - r));

      input_lengths[i] = 1;
      consume(i, c);
      giveup = true;
    }
  }
  if (giveup)
    return 0;

  // merge the input dictionaries to the output dictionary
  if (input_dicts.size() > 0) {
    output_dict = input_dicts[0];
    for (int i = 1; i < input_dicts.size(); i++)
      output_dict = pmt::dict_update(output_dict, input_dicts[i]);
  } else
    output_dict = PMT_EMPTY_DICT;

  // do the work
  int prod = work(max_output_len, input_lengths, input_items, output_items);
  if (prod == WORK_DONE || prod == WORK_CALLED_PRODUCE) {
    for (int i = 0; i < ninput_items.size(); i++)
      input_lengths[i] = 1; // reset the input size
    return prod;
  }

  for (int i = 0; i < ninput_items.size(); i++) {
    consume(i, input_lengths[i]);
    input_lengths[i] = 1; // reset the input size
  }

  // you should not produce more than configured to
  if (prod < 0 || prod > max_output_len) {
    std::cout << "####### tagged_stream_block2: produced more than allowed at "
              << alias() << std::endl;
    return 0;
  }

  if (prod > 0 && output_items.size() > 0) {
    for (int i = 0; i < output_items.size(); i++)
      produce_manually(i, prod);
  }
  return WORK_CALLED_PRODUCE;
}

void tagged_stream_block2::produce_manually(int output, int produced) {
  add_item_tag(output, nitems_written(output), PMT_PACKET_LEN,
               pmt::from_long(produced), block_name_sym);
  add_item_tag(output, nitems_written(output), PMT_PACKET_DICT, output_dict,
               block_name_sym);
  produce(output, produced);
}

bool tagged_stream_block2::start() {
  for (int i = 0; i < detail()->noutputs(); i++) {
    std::stringstream msg;
    msg << alias() << " output " << i << " buffer items "
        << max_output_buffer(i) << std::endl;
    std::cout << msg.str();
  }

  return block::start();
}

} /* namespace marmote3 */
} /* namespace gr */
