/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "test_message_source_impl.h"
#include <boost/crc.hpp>
#include <chrono>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/headers.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

test_message_source::sptr
test_message_source::make(int payload_min, int payload_max, int framing_type,
                          int message_num, int delay_first, int delay_next,
                          int src_radio_id, int dst_radio_id) {
  return gnuradio::get_initial_sptr(new test_message_source_impl(
      payload_min, payload_max, framing_type, message_num, delay_first,
      delay_next, src_radio_id, dst_radio_id));
}

test_message_source_impl::test_message_source_impl(
    int payload_min, int payload_max, int framing_type, int message_num,
    int delay_first, int delay_next, int src_radio_id, int dst_radio_id)
    : gr::block("test_message_source", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      payload_min(payload_min), payload_max(payload_max),
      framing_type(framing_type), message_num(message_num),
      delay_first(delay_first), delay_next(delay_next),
      src_radio_id(src_radio_id), dst_radio_id(dst_radio_id),
      out_port(pmt::mp("out")), rand_len(payload_min, payload_max),
      rand_byte(0, 255), thread(NULL) {
  if (payload_min < 0)
    throw std::out_of_range(
        "test_message_source: payload min length cannot be negative");

  if (payload_max < payload_min)
    throw std::out_of_range("test_message_source: payload max length cannot "
                            "be smaller than min length");

  if (framing_type < 0 || framing_type > 4)
    throw std::out_of_range("test_message_source: invalid framing type");

  if (framing_type == 1 && payload_max > 1500)
    throw std::out_of_range(
        "test_message_source: max payload is 1500 for Ethernet");

  if (framing_type == 2 && payload_max > 1480)
    throw std::out_of_range(
        "test_message_source: max payload is 1480 for IPv4");

  if (message_num < 0)
    throw std::out_of_range(
        "test_message_source: message num cannot be negative");

  if (delay_first < 0)
    throw std::out_of_range(
        "test_message_source: delay first cannot be negative");

  if (delay_next < 0)
    throw std::out_of_range(
        "test_message_source: delay next cannot be negative");

  if (src_radio_id < 0 || src_radio_id > 255)
    throw std::out_of_range("test_message_source: invalid src radio id");

  if (dst_radio_id < 0 || dst_radio_id > 255)
    throw std::out_of_range("test_message_source: invalid dst radio id");

  message_port_register_out(out_port);
}

void test_message_source_impl::thread_stop() {
  if (thread) {
    thread->interrupt();
    thread->join();
    thread = NULL;
  }
}

test_message_source_impl::~test_message_source_impl() { thread_stop(); }

bool test_message_source_impl::start() {
  thread_stop();
  thread = boost::shared_ptr<boost::thread>(new boost::thread(
      boost::bind(&test_message_source_impl::thread_work, this)));
  return block::start();
}

bool test_message_source_impl::stop() {
  thread_stop();

  gr::thread::scoped_lock lock(mutex);
  std::stringstream str;
  str << alias() << ": total produced " << total_produced << std::endl;
  std::cout << str.str();

  return block::stop();
}

const uint8_t SRC_MAC[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 101};
const uint8_t DST_MAC[6] = {0x12, 0x34, 0x56, 0x78, 0x90, 102};
const uint8_t SRC_IP[4] = {192, 168, 101, 2};
const uint8_t DST_IP[4] = {192, 168, 102, 2};

void test_message_source_impl::add_payload(std::vector<uint8_t> &packet,
                                           int payload_len) {
  assert(0 <= payload_len);
  int crc_len = std::min(payload_len, 2);
  payload_len -= crc_len;

  for (int i = 0; i < payload_len; i++)
    packet.push_back(rand_byte(rand_dev));

  boost::crc_16_type crc16;
  crc16.process_bytes(packet.data() + packet.size() - payload_len, payload_len);
  crc16.process_bytes(packet.data(), packet.size() - payload_len);
  uint16_t crc = crc16.checksum() ^ 0x1723;
  if (crc_len >= 1)
    packet.push_back(crc);
  if (crc_len == 2)
    packet.push_back(crc >> 8);
}

void test_message_source_impl::thread_work() {
  if (delay_first > 0)
    boost::this_thread::sleep(boost::posix_time::milliseconds(delay_first));

  while (!boost::this_thread::interruption_requested()) {
    if (message_num > 0 && total_produced >= message_num)
      break;

    int payload_len = rand_len(rand_dev);
    pmt::pmt_t bytes;

    if (framing_type == 0) {
      std::vector<uint8_t> packet;
      add_payload(packet, payload_len);

      bytes = pmt::init_u8vector(packet.size(), packet.data());
    }
    if (framing_type == 1) {
      eth_header_t eth;
      std::memcpy(eth.dst, DST_MAC, 6);
      std::memcpy(eth.src, SRC_MAC, 6);
      eth.dst[5] = dst_radio_id;
      eth.src[5] = src_radio_id;
      eth.typ = 0x08fe; // unused ethertype

      std::vector<uint8_t> packet;
      packet.resize(eth.eth_raw_len());
      int a = eth.eth_raw_save(packet.data());
      assert(0 <= a && a == packet.size());
      ((void)(a)); // avoid unused variable warning
      add_payload(packet, payload_len);

      bytes = pmt::init_u8vector(packet.size(), packet.data());
    } else if (framing_type == 2) {
      eth_header_t eth;
      std::memcpy(eth.dst, DST_MAC, 6);
      std::memcpy(eth.src, SRC_MAC, 6);
      eth.dst[5] = dst_radio_id;
      eth.src[5] = src_radio_id;
      eth.typ = 0x0800; // ipv4

      ip4_header_t ip4;
      ip4.tos = rand_byte(rand_dev) & 0x13;
      ip4.tlen = ip4.ip4_raw_len() + payload_len;
      ip4.id = total_produced;
      ip4.frag = 0x4000;
      ip4.ttl = 254;
      ip4.prot = 0xfe; // unused protocol
      std::memcpy(ip4.src, SRC_IP, 4);
      std::memcpy(ip4.dst, DST_IP, 4);
      ip4.src[2] = src_radio_id;
      ip4.dst[2] = dst_radio_id;

      std::vector<uint8_t> packet;
      packet.resize(eth.eth_raw_len() + ip4.ip4_raw_len());
      int a = eth.eth_raw_save(packet.data());
      assert(0 <= a && a == eth.eth_raw_len());
      int b = ip4.ip4_raw_save(packet.data() + a);
      assert(0 <= b && b == ip4.ip4_raw_len());
      ((void)(b)); // avoid unused variable warning
      add_payload(packet, payload_len);

      bytes = pmt::init_u8vector(packet.size(), packet.data());
    } else if (framing_type == 3) {
      eth_header_t eth;
      std::memcpy(eth.dst, DST_MAC, 6);
      std::memcpy(eth.src, SRC_MAC, 6);
      eth.dst[5] = dst_radio_id;
      eth.src[5] = src_radio_id;
      eth.typ = 0x0800; // ipv4

      ip4_header_t ip4;
      ip4.tos = rand_byte(rand_dev) & 0x13;
      ip4.tlen = ip4.ip4_raw_len() + udp_header_t::udp_raw_len() + payload_len;
      ip4.id = total_produced;
      ip4.frag = 0x4000;
      ip4.ttl = 254;
      ip4.prot = 0x11; // UDP
      std::memcpy(ip4.src, SRC_IP, 4);
      std::memcpy(ip4.dst, DST_IP, 4);
      ip4.src[2] = src_radio_id;
      ip4.dst[2] = dst_radio_id;

      udp_header_t udp;
      udp.src_port = 4000 + (rand_byte(rand_dev) & 3);
      udp.dst_port = 1000 + udp.src_port;
      udp.ulen = udp_header_t::udp_raw_len() + payload_len;
      udp.crc = (rand_byte(rand_dev) << 8) + rand_byte(rand_dev);

      std::vector<uint8_t> packet;
      packet.resize(eth.eth_raw_len() + ip4.ip4_raw_len() +
                    udp_header_t::udp_raw_len());
      int a = eth.eth_raw_save(packet.data());
      assert(0 <= a && a == eth.eth_raw_len());
      int b = ip4.ip4_raw_save(packet.data() + a);
      assert(0 <= b && b == ip4.ip4_raw_len());
      udp.udp_raw_save(packet.data() + a + b);
      add_payload(packet, payload_len);

      bytes = pmt::init_u8vector(packet.size(), packet.data());
    } else if (framing_type == 4) {
      eth_header_t eth;
      std::memcpy(eth.dst, DST_MAC, 6);
      std::memcpy(eth.src, SRC_MAC, 6);
      eth.dst[5] = dst_radio_id;
      eth.src[5] = src_radio_id;
      eth.typ = 0x0800; // ipv4

      ip4_header_t ip4;
      ip4.tos = rand_byte(rand_dev) & 0x13;
      ip4.tlen = ip4.ip4_raw_len() + udp_header_t::udp_raw_len() +
                 (rand_byte(rand_dev) > 1 ? 53 : 52) + payload_len;
      ip4.id = total_produced;
      // ip4.frag = 0x4000 + (rand_byte(rand_dev) & 1);
      ip4.frag = 0x4000;
      ip4.ttl = 254;
      ip4.prot = 0x11; // UDP
      std::memcpy(ip4.src, SRC_IP, 4);
      std::memcpy(ip4.dst, DST_IP, 4);
      ip4.src[2] = src_radio_id;
      ip4.dst[2] = dst_radio_id;

      udp_header_t udp;
      udp.src_port = 4000 + (rand_byte(rand_dev) & 1);
      udp.dst_port = 5000 + (rand_byte(rand_dev) & 1);
      udp.ulen = udp_header_t::udp_raw_len() + payload_len;
      udp.crc = (rand_byte(rand_dev) << 8) + rand_byte(rand_dev);

      std::vector<uint8_t> packet;
      packet.resize(eth.eth_raw_len() + ip4.ip4_raw_len() +
                    udp_header_t::udp_raw_len());
      int a = eth.eth_raw_save(packet.data());
      assert(0 <= a && a == eth.eth_raw_len());
      int b = ip4.ip4_raw_save(packet.data() + a);
      assert(0 <= b && b == ip4.ip4_raw_len());
      udp.udp_raw_save(packet.data() + a + b);

      std::chrono::system_clock::duration now =
          std::chrono::system_clock::now().time_since_epoch();
      int seconds =
          std::chrono::duration_cast<std::chrono::seconds>(now).count();
      int microsecs = std::chrono::duration_cast<std::chrono::microseconds>(
                          now - std::chrono::seconds(seconds))
                          .count();

      // generate random mgen like payload
      std::vector<uint8_t> mgen;
      add_payload(mgen, 53);
      mgen[2] = rand_byte(rand_dev) > 10 ? 4 : 5;  // mgenVersion
      mgen[3] = rand_byte(rand_dev) > 10 ? 12 : 0; // mgenFlags
      mgen[4] = 0;                                 // flowid
      mgen[5] = 0;                                 //
      mgen[6] = udp.dst_port >> 8;                 //
      mgen[7] = udp.dst_port;                      //
      mgen[8] = total_produced >> 24;              // seqno
      mgen[9] = total_produced >> 16;              //
      mgen[10] = total_produced >> 8;              //
      mgen[11] = total_produced;                   //
      mgen[12] = 0;                                // mgenFrag
      mgen[13] = 0;                                //
      mgen[14] = 0;                                //
      mgen[15] = rand_byte(rand_dev) & 8;          //
      mgen[16] = seconds >> 24;                    // seconds
      mgen[17] = seconds >> 16;                    //
      mgen[18] = seconds >> 8;                     //
      mgen[19] = seconds;                          //
      mgen[20] = microsecs >> 24;                  // microsecs
      mgen[21] = microsecs >> 16;                  //
      mgen[22] = microsecs >> 8;                   //
      mgen[23] = microsecs;                        //
      mgen[24] = udp.dst_port >> 8;                // dst port
      mgen[25] = udp.dst_port;                     //
      mgen[26] = 1;                                // dstAddrType
      mgen[27] = 4;                                // dstAddrLen
      mgen[28] = ip4.dst[0];                       // dstAddr
      mgen[29] = ip4.dst[1];                       //
      mgen[30] = ip4.dst[2];                       //
      mgen[31] = ip4.dst[3];                       //
      mgen[32] = 0;                                // hostData
      mgen[33] = 0;                                //
      mgen[34] = 0;                                //
      mgen[35] = 0;                                //
      mgen[36] = ip4.tos;                          // mgenTos
      mgen[37] = 0x04;                             // latitude
      mgen[38] = 0x37;                             //
      mgen[39] = 0x68;                             //
      mgen[40] = 0x20;                             //
      mgen[41] = 0x04;                             // longitude
      mgen[42] = 0x37;                             //
      mgen[43] = 0x68;                             //
      mgen[44] = 0x20 & rand_byte(rand_dev);       //
      mgen[45] = 0xff;                             // altitude
      mgen[46] = 0xff;                             //
      mgen[47] = 0xfc;                             //
      mgen[48] = 0x19;                             //
      mgen[49] = 0x00;                             // gpsStatus
      mgen[50] = 0x00;                             //
      mgen[51] = 0x00;                             //
      mgen[52] = 0x00;                             //

      packet.insert(packet.end(), mgen.begin(), mgen.end());
      add_payload(packet, std::max(payload_len - (int)mgen.size(), 0));

      bytes = pmt::init_u8vector(packet.size(), packet.data());
    }

    pmt::pmt_t msg = pmt::cons(PMT_EMPTY_DICT, bytes);
    message_port_pub(out_port, msg);

    if (false) {
      size_t len(0);
      const uint8_t *buffer = pmt::u8vector_elements(pmt::cdr(msg), len);

      std::stringstream str;
      str << alias() << ": sent packet  0x" << std::right << std::setfill('0')
          << std::hex;
      for (int i = 0; i < std::min((int)len, 100); i++)
        str << std::setw(2) << (unsigned int)buffer[i];
      str << std::endl;
      std::cout << str.str();
    }

    {
      gr::thread::scoped_lock lock(mutex);
      total_produced += 1;
      produced += 1;
    }

    if (delay_next > 0)
      boost::this_thread::sleep(boost::posix_time::milliseconds(delay_next));
  }
}

void test_message_source_impl::collect_block_report(ModemReport *modem_report) {
  TestMessageSource *report = modem_report->mutable_test_message_source();
  gr::thread::scoped_lock lock(mutex);

  report->set_produced(produced);
  produced = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
