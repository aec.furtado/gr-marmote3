/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "transpose_vxx_impl.h"
#include <cassert>
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

transpose_vxx::sptr transpose_vxx::make(int item_size, int in_vlen,
                                        int out_vlen) {
  return gnuradio::get_initial_sptr(
      new transpose_vxx_impl(item_size, in_vlen, out_vlen));
}

transpose_vxx_impl::transpose_vxx_impl(int item_size, int in_vlen, int out_vlen)
    : gr::block("transpose_vxx",
                gr::io_signature::make(1, 1, item_size * in_vlen),
                gr::io_signature::make(1, 1, item_size * out_vlen)),
      item_size(item_size), in_vlen(in_vlen), out_vlen(out_vlen),
      block_size(item_size * in_vlen * out_vlen) {
  if (in_vlen <= 0 || out_vlen <= 0)
    throw std::invalid_argument(
        "transpose_vxx: vector lengths must be positive");

  if (item_size != 1 && item_size != 2 && item_size != 4 && item_size != 8)
    throw std::invalid_argument("transpose_vxx: unsupported item size");

  set_relative_rate(((double)in_vlen) / out_vlen);
  set_fixed_rate(true);
  set_output_multiple(in_vlen);
}

transpose_vxx_impl::~transpose_vxx_impl() {}

int transpose_vxx_impl::fixed_rate_ninput_to_noutput(int ninput) {
  return ninput * in_vlen / out_vlen;
}

int transpose_vxx_impl::fixed_rate_noutput_to_ninput(int noutput) {
  return noutput * out_vlen / in_vlen;
}

void transpose_vxx_impl::forecast(int noutput_items,
                                  gr_vector_int &ninput_items_required) {
  ninput_items_required[0] = fixed_rate_noutput_to_ninput(noutput_items);
}

template <class elem_t>
void transpose_vxx_impl::transpose_4x4(const elem_t *in, elem_t *out) {
  elem_t a[4] = {in[0], in[1], in[2], in[3]};
  in += in_vlen;
  elem_t b[4] = {in[0], in[1], in[2], in[3]};
  in += in_vlen;
  elem_t c[4] = {in[0], in[1], in[2], in[3]};
  in += in_vlen;
  elem_t d[4] = {in[0], in[1], in[2], in[3]};

  out[0] = a[0];
  out[1] = b[0];
  out[2] = c[0];
  out[3] = d[0];
  out += out_vlen;
  out[0] = a[1];
  out[1] = b[1];
  out[2] = c[1];
  out[3] = d[1];
  out += out_vlen;
  out[0] = a[2];
  out[1] = b[2];
  out[2] = c[2];
  out[3] = d[2];
  out += out_vlen;
  out[0] = a[3];
  out[1] = b[3];
  out[2] = c[3];
  out[3] = d[3];
}

template <class elem_t>
void transpose_vxx_impl::transpose(const elem_t *in, elem_t *out) {
  int c = 0;

  while (c + 4 <= out_vlen) {
    int r = 0;

    while (r + 4 <= in_vlen) {
      transpose_4x4(in, out);
      r += 4;
      in += 4;
      out += 4 * out_vlen;
    }

    while (r < in_vlen) {
      out[0] = in[0];
      out[1] = in[in_vlen];
      out[2] = in[2 * in_vlen];
      out[3] = in[3 * in_vlen];

      r += 1;
      in += 1;
      out += out_vlen;
    }

    c += 4;
    in += 3 * in_vlen;
    out += 4 - in_vlen * out_vlen;
  }

  while (c < out_vlen) {
    int r = 0;

    while (r < in_vlen) {
      *out = *in;
      r += 1;
      in += 1;
      out += out_vlen;
    }

    c += 1;
    out += 1 - in_vlen * out_vlen;
  }
}

int transpose_vxx_impl::general_work(int noutput_items,
                                     gr_vector_int &ninput_items,
                                     gr_vector_const_void_star &input_items,
                                     gr_vector_void_star &output_items) {
  assert(noutput_items % in_vlen == 0);

  const char *in = (const char *)input_items[0];
  char *out = (char *)output_items[0];
  for (int n = 0; n < noutput_items; n += in_vlen) {
    switch (item_size) {
    case 1:
      transpose((const uint8_t *)in, (uint8_t *)out);
      break;
    case 2:
      transpose((const uint16_t *)in, (uint16_t *)out);
      break;
    case 4:
      transpose((const uint32_t *)in, (uint32_t *)out);
      break;
    case 8:
      transpose((const uint64_t *)in, (uint64_t *)out);
      break;
    }
    in += block_size;
    out += block_size;
  }

  consume_each(fixed_rate_noutput_to_ninput(noutput_items));
  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
