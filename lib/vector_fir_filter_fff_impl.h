/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_VECTOR_FIR_FILTER_FFF_IMPL_H
#define INCLUDED_MARMOTE3_VECTOR_FIR_FILTER_FFF_IMPL_H

#include <marmote3/vector_fir_filter_fff.h>

namespace gr {
namespace marmote3 {

class vector_fir_filter_fff_impl : public vector_fir_filter_fff {
private:
  const int vlen;
  const std::vector<float> taps;

public:
  vector_fir_filter_fff_impl(int vlen, const std::vector<float> &taps);
  ~vector_fir_filter_fff_impl();

  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

private:
  void work_cpu_a(const float *in, float *out);
  void work_cpu_b(const float *in, float *out);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_VECTOR_FIR_FILTER_FFF_IMPL_H */
