/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "vector_peak_probe_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>
#include <pmmintrin.h>
#include <sstream>

namespace gr {
namespace marmote3 {

vector_peak_probe::sptr vector_peak_probe::make(int vlen, int report_type,
                                                int histogram_bins,
                                                float histogram_low_db,
                                                float histogram_step_db) {
  return gnuradio::get_initial_sptr(new vector_peak_probe_impl(
      vlen, report_type, histogram_bins, histogram_low_db, histogram_step_db));
}

static const char *get_peak_probe_name(int report_type) {
  if (report_type == 1)
    return "vector_peak_probe_rx";
  else if (report_type == 2)
    return "vector_peak_probe_tx";
  else
    return "vector_peak_probe";
}

vector_peak_probe_impl::vector_peak_probe_impl(int vlen, int report_type,
                                               int histogram_bins,
                                               float histogram_low_db,
                                               float histogram_step_db)
    : gr::sync_block(get_peak_probe_name(report_type),
                     gr::io_signature::make(1, 1, vlen * sizeof(gr_complex)),
                     gr::io_signature::make(0, 0, 0)),
      vlen(vlen), report_type(report_type), histogram_bins(histogram_bins),
      histogram_low_db(histogram_low_db), histogram_step_db(histogram_step_db),
      carriers(vlen), samples(1), temp(2 * vlen), temp_bins(vlen),
      stalled_count(0) {
  if (vlen <= 0)
    throw std::invalid_argument(
        "vector_peak_probe: vector length must be positive");

  if (histogram_bins < 0)
    throw std::invalid_argument(
        "vector_peak_probe: histogram bins cannot be degative");

  if (histogram_bins != 0 && histogram_step_db <= 0.0)
    throw std::invalid_argument(
        "vector_peak_probe: histogram step must be positive");

  for (carrier_t &carrier : carriers)
    carrier.histogram.resize(histogram_bins, 0);
}

vector_peak_probe_impl::~vector_peak_probe_impl() {}

int vector_peak_probe_impl::work(int noutput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  const gr_complex *input = (const gr_complex *)input_items[0];

  int index = 0;
  while (index + 12 <= vlen) {
    const float *in = (float *)(input + index);

    __m128 m0 = _mm_setzero_ps();
    __m128 m1 = _mm_setzero_ps();
    __m128 m2 = _mm_setzero_ps();
    __m128 p0 = _mm_setzero_ps();
    __m128 p1 = _mm_setzero_ps();
    __m128 p2 = _mm_setzero_ps();

    for (int i = 0; i < noutput_items; i++) {
      __m128 a0 = _mm_loadu_ps(in);
      a0 = _mm_mul_ps(a0, a0);
      __m128 a1 = _mm_loadu_ps(in + 4);
      a1 = _mm_mul_ps(a1, a1);
      __m128 a2 = _mm_loadu_ps(in + 8);
      a2 = _mm_mul_ps(a2, a2);
      __m128 a3 = _mm_loadu_ps(in + 12);
      a3 = _mm_mul_ps(a3, a3);
      __m128 a4 = _mm_loadu_ps(in + 16);
      a4 = _mm_mul_ps(a4, a4);
      __m128 a5 = _mm_loadu_ps(in + 20);
      a5 = _mm_mul_ps(a5, a5);

      __m128 d0 = _mm_hadd_ps(a0, a1); // 4 magnitudes
      __m128 d1 = _mm_hadd_ps(a2, a3); // 4 magnitudes
      __m128 d2 = _mm_hadd_ps(a4, a5); // 4 magnitudes
      m0 = _mm_max_ps(m0, d0);
      p0 = _mm_add_ps(p0, d0);
      m1 = _mm_max_ps(m1, d1);
      p1 = _mm_add_ps(p1, d1);
      m2 = _mm_max_ps(m2, d2);
      p2 = _mm_add_ps(p2, d2);

      in += 2 * vlen;
    }

    _mm_storeu_ps(temp.data() + index, m0);
    _mm_storeu_ps(temp.data() + index + 4, m1);
    _mm_storeu_ps(temp.data() + index + 8, m2);
    _mm_storeu_ps(temp.data() + vlen + index, p0);
    _mm_storeu_ps(temp.data() + vlen + index + 4, p1);
    _mm_storeu_ps(temp.data() + vlen + index + 8, p2);
    index += 12;
  }

  while (index + 8 <= vlen) {
    const float *in = (float *)(input + index);

    __m128 m0 = _mm_setzero_ps();
    __m128 m1 = _mm_setzero_ps();
    __m128 p0 = _mm_setzero_ps();
    __m128 p1 = _mm_setzero_ps();

    for (int i = 0; i < noutput_items; i++) {
      __m128 a0 = _mm_loadu_ps(in);
      a0 = _mm_mul_ps(a0, a0);
      __m128 a1 = _mm_loadu_ps(in + 4);
      a1 = _mm_mul_ps(a1, a1);
      __m128 a2 = _mm_loadu_ps(in + 8);
      a2 = _mm_mul_ps(a2, a2);
      __m128 a3 = _mm_loadu_ps(in + 12);
      a3 = _mm_mul_ps(a3, a3);

      __m128 d0 = _mm_hadd_ps(a0, a1); // 4 magnitudes
      __m128 d1 = _mm_hadd_ps(a2, a3); // 4 magnitudes
      m0 = _mm_max_ps(m0, d0);
      p0 = _mm_add_ps(p0, d0);
      m1 = _mm_max_ps(m1, d1);
      p1 = _mm_add_ps(p1, d1);

      in += 2 * vlen;
    }

    _mm_storeu_ps(temp.data() + index, m0);
    _mm_storeu_ps(temp.data() + index + 4, m1);
    _mm_storeu_ps(temp.data() + vlen + index, p0);
    _mm_storeu_ps(temp.data() + vlen + index + 4, p1);
    index += 8;
  }

  while (index + 4 <= vlen) {
    const float *in = (float *)(input + index);

    __m128 m = _mm_setzero_ps();
    __m128 p = _mm_setzero_ps();

    for (int i = 0; i < noutput_items; i++) {
      __m128 a0 = _mm_loadu_ps(in);
      __m128 a1 = _mm_loadu_ps(in + 4);
      a0 = _mm_mul_ps(a0, a0);
      a1 = _mm_mul_ps(a1, a1);

      __m128 d = _mm_hadd_ps(a0, a1); // 4 magnitudes
      m = _mm_max_ps(m, d);
      p = _mm_add_ps(p, d);

      in += 2 * vlen;
    }

    _mm_storeu_ps(temp.data() + index, m);
    _mm_storeu_ps(temp.data() + vlen + index, p);
    index += 4;
  }

  while (index + 1 <= vlen) {
    const float *in = (float *)(input + index);

    float m = 0.0f;
    float p = 0.0f;

    for (int i = 0; i < noutput_items; i++) {
      float a0 = in[0];
      float a1 = in[1];

      float d = a0 * a0 + a1 * a1;
      m = std::fmax(m, d);
      p += d;

      in += 2 * vlen;
    }

    temp[index] = m;
    temp[vlen + index] = p;
    index += 1;
  }

  if (histogram_bins > 0) {
    float factor = noutput_items <= 0 ? 0.0f : 1.0f / noutput_items;
    for (int i = 0; i < vlen; i++) {
      float db = 10.0f * std::log10(std::max(temp[vlen + i] * factor, 1e-40f));
      int bin = std::ceil((db - histogram_low_db) / histogram_step_db);
      temp_bins[i] = std::max(std::min(bin, histogram_bins - 1), 0);
    }
  }

  // update it atomically
  gr::thread::scoped_lock lock(mutex);
  samples += noutput_items;
  for (int i = 0; i < vlen; i++) {
    carrier_t &carrier = carriers[i];
    carrier.maximum = std::max(carrier.maximum, temp[i]);
    carrier.energy += temp[vlen + i];
    if (histogram_bins > 0) {
      carrier.last_bin = temp_bins[i];
      carrier.histogram[temp_bins[i]] += 1;
    }
  }

  return noutput_items;
}

void vector_peak_probe_impl::collect_block_report(ModemReport *modem_report) {
  gr::thread::scoped_lock lock(mutex);

  if (samples <= 0) {
    if (++stalled_count >= 3) {
      std::stringstream msg;
      msg << alias() << ": stalled, restarting modem" << std::endl;
      std::cout << msg.str();
      std::exit(-1);
    }
  } else
    stalled_count = 0;

  PeakProbeReport *report;
  if (report_type == 1)
    report = modem_report->mutable_receive_peak_probe();
  else if (report_type == 2)
    report = modem_report->mutable_transmit_peak_probe();
  else
    return;

  report->set_samples(samples);
  report->set_histogram_low_db(histogram_low_db);
  report->set_histogram_step_db(histogram_step_db);
  for (carrier_t &carrier : carriers) {
    float average_db =
        10.0 * std::log10(std::max(carrier.energy / samples, 1e-40));
    float maximum_db = 10.0f * std::log10(std::max(carrier.maximum, 1e-40f));

    PeakProbeCarrier *report2 = report->add_carriers();
    report2->set_average_db(average_db);
    report2->set_maximum_db(maximum_db);
    if (histogram_bins > 0) {
      *report2->mutable_histogram() = {carrier.histogram.begin(),
                                       carrier.histogram.end()};
      carrier.histogram.assign(histogram_bins, 0);
    }

    carrier.energy = 0.0;
    carrier.maximum = 0.0f;
  }
  samples = 0;
}

std::vector<float> vector_peak_probe_impl::get_maximum_db() {
  std::vector<float> result;
  result.reserve(vlen);

  gr::thread::scoped_lock lock(mutex);
  for (const carrier_t &carrier : carriers) {
    float maximum_db = 10.0f * std::log10(std::max(carrier.maximum, 1e-40f));
    result.push_back(maximum_db);
  }

  return result;
}

std::vector<unsigned int> vector_peak_probe_impl::get_power_bins() {
  std::vector<unsigned int> result;
  result.reserve(vlen);

  // aligned integer access is atomic, no lock
  for (const carrier_t &carrier : carriers)
    result.push_back(carrier.last_bin);

  return result;
}

} /* namespace marmote3 */
} /* namespace gr */
