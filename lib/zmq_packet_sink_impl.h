/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_ZMQ_PACKET_SINK_IMPL_H
#define INCLUDED_MARMOTE3_ZMQ_PACKET_SINK_IMPL_H

#include <chrono>
#include <marmote3/zmq_packet_sink.h>
#include <zmq.hpp>

namespace gr {
namespace marmote3 {

class zmq_packet_sink_impl : public zmq_packet_sink {
private:
  const int itemsize;
  const std::string addr;

  zmq::context_t context;
  zmq::socket_t socket;

  int success_count, failure_count;
  std::chrono::system_clock::time_point last_report;

public:
  zmq_packet_sink_impl(int itemsize, const std::string &addr, bool push);
  ~zmq_packet_sink_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_ZMQ_PACKET_SINK_IMPL_H */
