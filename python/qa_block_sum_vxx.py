#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_block_sum_vxx (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [1j, 2, 3j, 4, 5j, 6], False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, 3),
            marmote3.block_sum_vxx(6, 2, 2),
            blocks.vector_to_stream(gr.sizeof_gr_complex, 3),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(np.allclose(sink.data(), [1j + 4, 2 + 5j, 3j + 6]))

    def test2(self):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                [1j, 2, 3j, 4, 5j, 6], False),
            marmote3.block_sum_vxx(2, 3, 3),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(np.allclose(sink.data(), [1j + 2 + 3j, 4 + 5j + 6]))

    def test3(self):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f(range(32 * 100), False),
            blocks.stream_to_vector(gr.sizeof_float, 16),
            marmote3.block_sum_vxx(16, 2, 2),
            blocks.vector_to_stream(gr.sizeof_float, 16),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(len(sink.data()) == 16 * 100)

    def test4(self):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f([1, 2, 3, 4], False),
            marmote3.block_sum_vxx(1, 1, 2),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(np.allclose(sink.data(), [1, 3, 5, 7]))

    def test5(self):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f([1, 2, 3, 4], False),
            marmote3.block_sum_vxx(1, 2, 1),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(np.allclose(sink.data(), [1, 3]))


if __name__ == '__main__':
    gr_unittest.run(qa_block_sum_vxx, "qa_block_sum_vxx.xml")
