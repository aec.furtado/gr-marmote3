#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_chirp_frame_timing (gr_unittest.TestCase):

    def test_shape(self):
        chirp_len = 12
        fft_len = 4
        vlen = 5

        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f(np.zeros(2 * chirp_len * vlen), False),
            blocks.stream_to_vector(gr.sizeof_float, fft_len),
            marmote3.chirp_frame_timing(chirp_len, fft_len, vlen, 1e-9),
            blocks.vector_to_stream(gr.sizeof_float, fft_len),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(len(sink.data()) == 2 * fft_len * vlen)
        self.assertTrue(np.allclose(sink.data(), np.zeros(2 * fft_len * vlen)))

    def test_basic(self):
        chirp_len = 4
        fft_len = 2
        vlen = 1

        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f(
                [0, 1, 0.5, 0.5, 0, 0, 0, 0], False),
            blocks.stream_to_vector(gr.sizeof_float, fft_len),
            marmote3.chirp_frame_timing(chirp_len, fft_len, vlen, 1e-9),
            blocks.vector_to_stream(gr.sizeof_float, fft_len),
            sink
        )
        top.run()
        print sink.data()
        self.assertTrue(
            np.allclose(sink.data(), [0, 1.0, 0.5, 0]))


if __name__ == '__main__':
    gr_unittest.run(qa_chirp_frame_timing, "qa_chirp_frame_timing.xml")
