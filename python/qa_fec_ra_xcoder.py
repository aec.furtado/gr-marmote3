#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr_unittest
import marmote3_swig as marmote3
import random


class qa_fec_ra_xcoder (gr_unittest.TestCase):

    def test1(self):
        v = marmote3.fec_ra_permutation.generate_permutation(1000)
        self.assertEqual(len(v), 4000)
        c = [0] * 4000

        for i in range(4):
            for j in range(1000):
                k = v[i * 1000 + j]
                c[i * 1000 + k] += 1

        self.assertEqual(c, [1] * 4000)

    def test2(self):
        v = marmote3.fec_ra_permutation.generate_puncturing(15, 20)
        self.assertEqual(list(v), range(15) + range(15, 4 * 15, 9))

    def gen_test(self, data_len, code_len):
        encoder = marmote3.fec_ra_encoder(data_len, code_len)
        decoder = marmote3.fec_ra_decoder(data_len, code_len, 1, 15)

        orig_data = [random.randint(0, 255) for _ in range(data_len)]
        print(orig_data)
        orig_code = encoder.encode(orig_data)
        print(orig_code)
        soft_code = []
        for b in orig_code:
            for i in range(8):
                soft_code.append(-1 if (b >> i) & 1 != 0 else 1)
        print(soft_code)
        decoded = decoder.decode(soft_code)
        print(decoded)

        self.assertEqual(orig_data, list(decoded))

    def test3(self):
        self.gen_test(1, 1)

    def test4(self):
        self.gen_test(1, 2)

    def test5(self):
        self.gen_test(1, 3)

    def test6(self):
        self.gen_test(2, 2)

    def test7(self):
        self.gen_test(2, 3)

    def test8(self):
        self.gen_test(2, 4)


if __name__ == '__main__':
    gr_unittest.run(qa_fec_ra_xcoder, "qa_fec_ra_xcoder.xml")
