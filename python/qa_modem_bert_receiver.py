#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks, analog
import marmote3_swig as marmote3
import math
import numpy as np
from comm_performance import calc_mpsk_awgn_ber_vs_ebn0, calc_qam_awgn_ber_vs_ebn0


def simulate_ber(SNR, M, data_len=600, packet_num=20):
    top = gr.top_block()

    bert_sender = marmote3.modem_bert_sender(1, packet_num, data_len)
    rep_encoder = marmote3.packet_rep_encoder(data_len * 8, 1)
    symb_mod = marmote3.packet_symb_mod(M, data_len * 8)
    noise = analog.fastnoise_source_c(
        analog.GR_GAUSSIAN, 1.0 / math.sqrt(10**(float(SNR) / 10.0)), 0, 8192)
    add = blocks.add_vcc(1)
    symb_demod = marmote3.packet_symb_demod(M, 24, data_len * 8)
    rep_decoder = marmote3.packet_rep_decoder(data_len, 1)
    bert_receiver = marmote3.modem_bert_receiver(1)

    top.connect(bert_sender, rep_encoder, symb_mod, (add, 0))
    top.connect(noise, (add, 1))
    top.connect(add, symb_demod, rep_decoder, bert_receiver)
    top.run()
    return bert_receiver.get_ber()


class qa_modem_bert_receiver (gr_unittest.TestCase):

    def test1(self):  # BPSK
        snr = np.arange(0, 6)
        ber_sim = np.zeros((len(snr), ))
        for sn in range(len(snr)):
            ber_sim[sn] = simulate_ber(snr[sn], 1)

        ber_theor = calc_mpsk_awgn_ber_vs_ebn0(2, 10**(snr / 10.0))
        self.assertTrue(
            np.allclose(ber_theor, ber_sim, rtol=1e-2, atol=1e-2))

    def test2(self):  # QPSK
        snr = np.arange(0, 6)
        ber_sim = np.zeros((len(snr), ))
        for sn in range(len(snr)):
            ber_sim[sn] = simulate_ber(snr[sn], 2)

        ber_theor = calc_mpsk_awgn_ber_vs_ebn0(4, 10**(snr / 10.0) / 2)
        self.assertTrue(
            np.allclose(ber_theor, ber_sim, rtol=1e-2, atol=1e-2))

    def test3(self):  # 8PSK
        snr = np.arange(5, 10)
        ber_sim = np.zeros((len(snr), ))
        for sn in range(len(snr)):
            ber_sim[sn] = simulate_ber(snr[sn], 3)

        ber_theor = calc_mpsk_awgn_ber_vs_ebn0(8, 10**(snr / 10.0) / 3)
        self.assertTrue(
            np.allclose(ber_theor, ber_sim, rtol=1e-2, atol=1e-2))

    def test4(self):  # 16QAM
        snr = np.arange(5, 12)
        ber_sim = np.zeros((len(snr), ))
        for sn in range(len(snr)):
            ber_sim[sn] = simulate_ber(snr[sn], 4)

        ber_theor = calc_qam_awgn_ber_vs_ebn0(16, 10**(snr / 10.0) / 4)
        print ber_theor
        print ber_sim
        self.assertTrue(
            np.allclose(ber_theor, ber_sim, rtol=1e-1, atol=1e-1))


if __name__ == '__main__':
    gr_unittest.run(qa_modem_bert_receiver, "qa_modem_bert_receiver.xml")
