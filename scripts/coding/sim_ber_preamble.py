#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

"""
This module runs the full preamble and payload coding and modulation
pipline testing the packet error rates.
"""

import marmote3
import numpy as np
import matplotlib.pyplot as plt

from test_ber_preamble_racode import test_ber_preamble_racode
from test_ber_preamble_repeat import test_ber_preamble_repeat


def get_mod_name(mod_index):
    return [None, 'BPSK', 'QPSK', '8PSK', '16QAM', '32QAM', '64QAM'][mod_index]


def plot_ber_preamble_racode(snrs, mod_index, data_len, code_len):
    bers = np.zeros([len(snrs)])

    for idx, snr in enumerate(snrs):
        g = test_ber_preamble_racode(
            data_len=data_len,
            code_len=code_len,
            mod_index=mod_index,
            num_packets=100,
            full_passes=1,
            half_passes=15,
            snr=snr)
        g.run()
        bers[idx] = g.marmote3_modem_bert_receiver_0.get_ber()

    plt.semilogy(snrs, bers, label="{} RA {}/{}".format(
        get_mod_name(mod_index), data_len, code_len), marker="x")


def plot_fer_preamble_racode(snrs, mod_index, data_len, code_len):
    fers = np.zeros([len(snrs)])

    for idx, snr in enumerate(snrs):
        g = test_ber_preamble_racode(
            data_len=data_len,
            code_len=code_len,
            mod_index=mod_index,
            num_packets=200,
            full_passes=1,
            half_passes=15,
            snr=snr)
        g.run()
        fers[idx] = g.marmote3_modem_bert_receiver_0.get_fer()

    plt.semilogy(snrs, fers, label="{} RA {}/{}".format(
        get_mod_name(mod_index), data_len, code_len), marker="x")


def plot_ber_preamble_repeat(snrs, mod_index, data_len, repeat):
    bers = np.zeros([len(snrs)])

    for idx, snr in enumerate(snrs):
        g = test_ber_preamble_repeat(
            data_len=data_len,
            repeat=repeat,
            mod_index=mod_index,
            num_packets=100,
            snr=snr)
        g.run()
        bers[idx] = g.marmote3_modem_bert_receiver_0.get_ber()

    plt.semilogy(snrs, bers, label="{} repeat {}/{}".format(
        get_mod_name(mod_index), data_len, repeat * data_len), marker="o")


def plot_ber_theoretical(snrs, mod_index, repeat):
    if mod_index <= 3:
        bers = marmote3.calc_mpsk_awgn_ber_vs_ebn0(
            2 ** mod_index, repeat * 10**(snrs / 10.0) / mod_index)
    elif mod_index == 4:
        bers = marmote3.calc_qam_awgn_ber_vs_ebn0(
            2 ** mod_index, repeat * 10**(snrs / 10.0) / mod_index)

    plt.semilogy(snrs, bers, label="{} th. repeat {}".format(
        get_mod_name(mod_index), repeat), marker="v")


def default_plots():
    snrs = np.arange(-2, 10.5, 0.5)
    for i in [1, 2, 3, 4]:
        plot_ber_preamble_racode(snrs, i, 1500, 3000)
        plot_ber_preamble_repeat(snrs, i, 1500, 2)
        plot_ber_theoretical(snrs, i, 2)


def rate_fer_plots1():
    plt.figure()
    snrs = np.arange(4, 10.5, 0.5)
    plot_fer_preamble_racode(snrs, 1, 400, 800)  # rate 1/2 = 0.5
    plot_fer_preamble_racode(snrs, 2, 800, 1600)  # rate 1/2 = 0.5
    plot_fer_preamble_racode(snrs, 2, 1200, 1600)  # rate 3/4 = 0.75
    plot_fer_preamble_racode(snrs, 4, 1600, 3200)  # rate 1/2 = 0.5
    plot_fer_preamble_racode(snrs, 4, 2000, 3200)  # rate 5/8 = 0.625
    plot_fer_preamble_racode(snrs, 4, 2400, 3200)  # rate 3/4 = 0.75
    plt.xlabel('SNR')
    plt.ylabel('FER')
    plt.legend()
    plt.grid()
    plt.show()


def rate_fer_plots2():
    plt.figure()
    snrs = np.arange(0, 18.5, 0.5)
    plot_fer_preamble_racode(snrs, 1, 1000, 2 * int(0.5 * 1000 / 0.5))
    plot_fer_preamble_racode(snrs, 1, 1000, 2 * int(0.5 * 1000 / 0.625))
    plot_fer_preamble_racode(snrs, 1, 1000, 2 * int(0.5 * 1000 / 0.75))
    plot_fer_preamble_racode(snrs, 1, 1000, 2 * int(0.5 * 1000 / 0.8))
    plot_fer_preamble_racode(snrs, 1, 1000, 2 * int(0.5 * 1000 / 0.875))
    plot_fer_preamble_racode(snrs, 2, 1000, 2 * int(0.5 * 1000 / 0.5))
    plot_fer_preamble_racode(snrs, 2, 1000, 2 * int(0.5 * 1000 / 0.625))
    plot_fer_preamble_racode(snrs, 2, 1000, 2 * int(0.5 * 1000 / 0.75))
    plot_fer_preamble_racode(snrs, 2, 1000, 2 * int(0.5 * 1000 / 0.8))
    plot_fer_preamble_racode(snrs, 2, 1000, 2 * int(0.5 * 1000 / 0.875))
    plot_fer_preamble_racode(snrs, 4, 1000, 2 * int(0.5 * 1000 / 0.5))
    plot_fer_preamble_racode(snrs, 4, 1000, 2 * int(0.5 * 1000 / 0.625))
    plot_fer_preamble_racode(snrs, 4, 1000, 2 * int(0.5 * 1000 / 0.75))
    plot_fer_preamble_racode(snrs, 4, 1000, 2 * int(0.5 * 1000 / 0.8))
    plot_fer_preamble_racode(snrs, 4, 1000, 2 * int(0.5 * 1000 / 0.875))
    plot_fer_preamble_racode(
        snrs, 6, 1000, 6 * int((1.0 / 6) * 1000 / 0.66666))
    plot_fer_preamble_racode(snrs, 6, 1000, 6 * int((1.0 / 6) * 1000 / 0.75))
    plt.xlabel('SNR')
    plt.ylabel('FER')
    plt.legend()
    plt.grid()
    plt.show()


def size_fer_plots():
    plt.figure()
    snrs = np.arange(1, 5.4, 0.4)
    plot_fer_preamble_racode(snrs, 2, 100, 200)
    plot_fer_preamble_racode(snrs, 2, 200, 400)
    plot_fer_preamble_racode(snrs, 2, 400, 800)
    plot_fer_preamble_racode(snrs, 2, 800, 1600)
    plot_fer_preamble_racode(snrs, 2, 1600, 3200)
    plt.xlabel('SNR')
    plt.ylabel('FER')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    rate_fer_plots2()
