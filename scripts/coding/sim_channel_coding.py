#!/usr/bin/env python
#
# Copyright 2013-2018 Peter Horvath, Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

"""
This module runs the marmote3 viterbi and repeat accumulate codes directly
without embedding them into a flow graph. This just plots basic BER graphs.
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import marmote3


def get_standard_polys(rate, k):
    return marmote3.fec_conv_encoder.get_standard_polys(rate, k)


class Coder(object):

    def __init__(self, name, data_len):
        self.data_len = int(data_len)
        code = self.encode([0] * data_len)
        self.code_len = len(code)
        self.name = "{} {}/{}".format(name, self.data_len, self.code_len)
        self.rate = float(self.data_len) / self.code_len
        self.signal_power = np.mean(np.square(code))

    def encode(self, data_bits):
        raise NotImplementedError()

    def decode(self, soft_bits):
        raise NotImplementedError()

    def print_stats(self):
        print("{} data_len={}, code_len={}, rate={}, power={}".format(
            self.name, self.data_len, self.code_len, self.rate, self.signal_power))

    def get_ber_snr(self, ebn0, max_steps):
        noise_power = math.sqrt(0.5 * self.signal_power / (10 ** (0.1 * ebn0)))
        bit_errors = 0

        steps = 0
        while bit_errors < 5000 and steps < max_steps:
            data1 = np.random.randint(2, size=self.data_len)
            code1 = self.encode(data1)
            code2 = code1 + noise_power * np.random.randn(self.code_len)
            data2 = self.decode(code2)
            bit_errors += np.count_nonzero(data1 != data2)
            steps += 1

        return float(bit_errors) / (self.data_len * steps)

    def get_ber_ebn0(self, snr, max_steps):
        return self.get_ber_snr(snr + 10 * math.log10(self.rate), max_steps)


class RepeatCoder(Coder):

    def __init__(self, repeat, data_len):
        self.repeat = repeat
        Coder.__init__(self, "Repeat{}".format(repeat), data_len)

    def encode(self, data_bits):
        code_bits = np.repeat(data_bits, self.repeat)
        return 1.0 - 2.0 * code_bits  # convert to soft bits

    def decode(self, soft_bits):
        data_bits = np.sum(np.reshape(soft_bits, (-1, self.repeat)), axis=-1)
        return np.choose(data_bits < 0, [0, 1])


class ViterbiCoder(Coder):

    def __init__(self, polys, k, data_len):
        self.encoder = marmote3.fec_conv_encoder(polys, k)
        self.decoder = marmote3.fec_conv_decoder(polys, k, data_len)
        Coder.__init__(self, "Viterbi{}{}".format(len(polys), k), data_len)

    def encode(self, data_bits):
        code_bits = np.array(self.encoder.encode(data_bits))
        return 1.0 - 2.0 * code_bits  # convert to soft bits

    def decode(self, soft_bits):
        return self.decoder.decode(soft_bits)


class ViterbiTBCoder(Coder):

    def __init__(self, polys, k, data_len):
        self.encoder = marmote3.fec_conv_encoder(polys, k)
        self.decoder = marmote3.fec_conv_decoder(polys, k, data_len)
        Coder.__init__(self, "ViterbiTB{}{}".format(len(polys), k), data_len)

    def encode(self, data_bits):
        code_bits = np.array(self.encoder.encode_tb(data_bits))
        return 1.0 - 2.0 * code_bits  # convert to soft bits

    def decode(self, soft_bits):
        return self.decoder.decode_tb(soft_bits)


class RepeatAccCoder(Coder):

    def __init__(self, data_len, code_len, full_passes=1, half_passes=15, scale=24):
        assert data_len % 16 == 0 and code_len % 16 == 0
        self.scale = scale
        self.encoder = marmote3.fec_ra_encoder(data_len / 8, code_len / 8)
        self.decoder = marmote3.fec_ra_decoder(
            data_len / 8, code_len / 8, full_passes, half_passes)
        Coder.__init__(self, "RA ({} passes)".format(full_passes + half_passes),
                       data_len)

    def encode(self, data_bits):
        data_bytes = np.packbits(np.array(data_bits, dtype=np.uint8))
        code_bytes = self.encoder.encode(map(int, data_bytes))
        code_bits = np.unpackbits(np.array(code_bytes, dtype=np.uint8))
        code_bits = code_bits.reshape(-1, 8)[:, ::-1].reshape(-1)
        soft_bits = 1.0 - 2.0 * code_bits
        return soft_bits

    def decode(self, soft_bits):
        code_bits = np.clip(self.scale * soft_bits, -128, 127).astype(np.int8)
        data_bytes = self.decoder.decode(map(int, code_bits))
        data_bits = np.unpackbits(np.array(data_bytes, dtype=np.uint8))
        return data_bits


def plot_ber_ebn0(coders, ebn0s, max_steps=100000, theory=True):
    formats = ['k-o', 'b-o', 'r-o', 'g-o', 'c-o', 'm-o', 'y-o']
    plt.figure()
    for idx, coder in enumerate(coders):
        coder.print_stats()
        bers = [coder.get_ber_ebn0(ebn0, max_steps) for ebn0 in ebn0s]
        plt.semilogy(ebn0s, bers,
                     formats[idx % len(formats)], label=coder.name)
    if theory:
        bers = marmote3.calc_mpsk_awgn_ber_vs_ebn0(2, 10 ** (ebn0s / 10.0))
        plt.semilogy(ebn0s, bers, label='BPSK Theory')
    plt.xlabel("Eb/N0 (dB)")
    plt.ylabel("BER")
    plt.legend()
    plt.grid()
    plt.show()


def plot_ber_snr(coders, snrs, max_steps=100000, theory=True):
    formats = ['k-o', 'b-o', 'r-o', 'g-o', 'c-o', 'm-o', 'y-o']
    plt.figure()
    for idx, coder in enumerate(coders):
        coder.print_stats()
        bers = [coder.get_ber_snr(snr, max_steps) for snr in snrs]
        plt.semilogy(snrs, bers,
                     formats[idx % len(formats)], label=coder.name)
    if theory:
        bers = marmote3.calc_mpsk_awgn_ber_vs_ebn0(2, 10 ** (snrs / 10.0))
        plt.semilogy(snrs, bers, label='BPSK Theory')
    plt.xlabel("SNR (dB)")
    plt.ylabel("BER")
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == "__main__":
    plot_ber_ebn0([
        RepeatAccCoder(4096, 8192, full_passes=0, half_passes=16),
        RepeatAccCoder(4096, 8192, full_passes=1, half_passes=15),
        RepeatAccCoder(4096, 8192, full_passes=0, half_passes=32),
        RepeatAccCoder(4096, 8192, full_passes=1, half_passes=31),
        # RepeatCoder(1, 32),
        RepeatCoder(2, 32),
        # RepeatCoder(3, 32),
        # ViterbiTBCoder(get_standard_polys(2, 9), 9, 32),
        # ViterbiTBCoder(get_standard_polys(3, 9), 9, 32),
        # ViterbiTBCoder(get_standard_polys(4, 9), 9, 32),
        # ViterbiTBCoder(get_standard_polys(6, 9), 9, 32),
    ], np.arange(1.5, 3.1, 0.1), 10000)
