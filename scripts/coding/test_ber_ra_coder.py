#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Ber Ra Coder
# Generated: Sun Aug  4 20:34:10 2019
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3
import math


class test_ber_ra_coder(gr.top_block):

    def __init__(self, code_len=3000, data_len=1500, full_passes=1, half_passes=15, noise_power=0.0, num_packets=10000, seed=1):
        gr.top_block.__init__(self, "Test Ber Ra Coder")

        ##################################################
        # Parameters
        ##################################################
        self.code_len = code_len
        self.data_len = data_len
        self.full_passes = full_passes
        self.half_passes = half_passes
        self.noise_power = noise_power
        self.num_packets = num_packets
        self.seed = seed

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(marmote3.MOD_QPSK or 0, code_len * 4)
        self.marmote3_packet_symb_demod_0 = marmote3.packet_symb_demod(marmote3.MOD_QPSK or 0, 16, code_len*8)
        self.marmote3_packet_ra_encoder_0 = marmote3.packet_ra_encoder(-1 if False else data_len, code_len)
        self.marmote3_packet_ra_decoder_0 = marmote3.packet_ra_decoder(data_len, -1 if False else code_len, full_passes, half_passes)
        self.marmote3_modem_bert_sender_0 = marmote3.modem_bert_sender(seed, num_packets, data_len)
        self.marmote3_modem_bert_receiver_0 = marmote3.modem_bert_receiver(seed)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        (self.blocks_add_xx_0).set_min_output_buffer(1048576)
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, math.sqrt(noise_power), 0)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.marmote3_packet_symb_demod_0, 0))
        self.connect((self.marmote3_modem_bert_sender_0, 0), (self.marmote3_packet_ra_encoder_0, 0))
        self.connect((self.marmote3_packet_ra_decoder_0, 0), (self.marmote3_modem_bert_receiver_0, 0))
        self.connect((self.marmote3_packet_ra_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_demod_0, 0), (self.marmote3_packet_ra_decoder_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.blocks_add_xx_0, 0))

    def get_code_len(self):
        return self.code_len

    def set_code_len(self, code_len):
        self.code_len = code_len

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len

    def get_full_passes(self):
        return self.full_passes

    def set_full_passes(self, full_passes):
        self.full_passes = full_passes

    def get_half_passes(self):
        return self.half_passes

    def set_half_passes(self, half_passes):
        self.half_passes = half_passes

    def get_noise_power(self):
        return self.noise_power

    def set_noise_power(self, noise_power):
        self.noise_power = noise_power
        self.analog_noise_source_x_0.set_amplitude(math.sqrt(self.noise_power))

    def get_num_packets(self):
        return self.num_packets

    def set_num_packets(self, num_packets):
        self.num_packets = num_packets

    def get_seed(self):
        return self.seed

    def set_seed(self, seed):
        self.seed = seed


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--code-len", dest="code_len", type="intx", default=3000,
        help="Set code length [default=%default]")
    parser.add_option(
        "", "--data-len", dest="data_len", type="intx", default=1500,
        help="Set data length [default=%default]")
    parser.add_option(
        "", "--full-passes", dest="full_passes", type="intx", default=1,
        help="Set full passes [default=%default]")
    parser.add_option(
        "", "--half-passes", dest="half_passes", type="intx", default=15,
        help="Set half passes [default=%default]")
    parser.add_option(
        "", "--noise-power", dest="noise_power", type="eng_float", default=eng_notation.num_to_str(0.0),
        help="Set noise power [default=%default]")
    parser.add_option(
        "", "--num-packets", dest="num_packets", type="intx", default=10000,
        help="Set number of pkts [default=%default]")
    parser.add_option(
        "", "--seed", dest="seed", type="intx", default=1,
        help="Set seed [default=%default]")
    return parser


def main(top_block_cls=test_ber_ra_coder, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(code_len=options.code_len, data_len=options.data_len, full_passes=options.full_passes, half_passes=options.half_passes, noise_power=options.noise_power, num_packets=options.num_packets, seed=options.seed)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
