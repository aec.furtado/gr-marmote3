import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import marmote3

txtaps = marmote3.get_fbmc_taps8(20, 22, "tx")
w, h = scipy.signal.freqz(txtaps)

# measured
f1 = np.array([.1, .2, .3, .4, .5, .6, .7, .77, .8, .85, .93, 1.8, 2.92])
p1 = np.array(
    [0, 0, -0.5, -2, -3.2, -9, -27, -47, -53, -58, -65, -65.5, -66.4])

# mask
f2 = np.array([0, 0, -40, -65, -65])
p2 = np.array([0, 0.625, 0.75, 1, 2.5])

plt.figure()
plt.plot(w[:100] / np.pi * 12.5, 20 * np.log10(abs(h[:100]))
         - 20 * np.log10(max(np.abs(h))), 'r--')
plt.plot([0.625, 0.625], [-100, 0], 'b-.')
plt.plot(p2, f2, 'k-')

plt.xlabel('Offset from the carrier [MHz]')
plt.ylabel('TX spectrum mask [dBc]')
plt.grid()
plt.ylim([-80, 5])
plt.xlim([0, 2])
plt.show()
